package com.kodality.medipost.app.model.servicelist;

import com.kodality.medipost.app.model.Coding;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class ObservationDefinition {
  private Coding code;
  private boolean orderable;
  private String resultUnit;
  private Integer sequenceNo;
  private List<InputRequirement> inputRequirements;
  private Integer specimenDefinitionSequenceNo;
  private List<SpecimenGroupDefinition> specimenGroupDefinitions;
}