package com.kodality.medipost.app.model.ack;

public enum AckCode {
  ok,
  partial_ok,
  xml_schema_error,
  data_error,
  order_long_processing,
  restricted_message,
  unknown_message,
  other
}
