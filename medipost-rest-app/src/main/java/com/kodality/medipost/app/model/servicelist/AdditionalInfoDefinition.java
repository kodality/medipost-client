package com.kodality.medipost.app.model.servicelist;

import com.kodality.medipost.app.model.Coding;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class AdditionalInfoDefinition {
  private Coding code;
  private List<String> possibleValues;
}