package com.kodality.medipost.app.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class Coding {
  private String system;
  private String value;
  private String name;
  private String description;
  private String tAbbreviation;
  private String usableName;
}