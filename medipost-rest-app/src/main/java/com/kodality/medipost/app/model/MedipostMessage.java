package com.kodality.medipost.app.model;

import com.kodality.medipost.app.model.MessageType;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class MedipostMessage {
  private String sender;
  private String recipient;
  private MessageType messageType;
  private String messageId;
  private LocalDateTime messageTime;
  private String senderEmail;
}