package com.kodality.medipost.app.model.order;

public enum ResultType {
  quantity,
  narrative,
  coded,
  timepoint
}
