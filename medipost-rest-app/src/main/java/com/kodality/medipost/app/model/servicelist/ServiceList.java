package com.kodality.medipost.app.model.servicelist;

import com.kodality.medipost.app.model.order.Organization;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class ServiceList {
  private Organization performer;
  private List<InputParameterDefinition> inputParameters;
  private List<ObservationGroupDefinition> observationGroups;
}