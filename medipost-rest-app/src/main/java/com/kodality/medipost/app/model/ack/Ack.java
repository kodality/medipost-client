package com.kodality.medipost.app.model.ack;

import com.kodality.medipost.app.model.MessageType;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class Ack {
  private AckCode code;
  private String note;
  private MessageType inResponseOfMessageType;
  private String inResponseOfMessageId;
  private String inResponseOfMessage;
}