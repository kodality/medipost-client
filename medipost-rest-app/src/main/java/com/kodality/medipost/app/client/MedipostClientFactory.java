package com.kodality.medipost.app.client;

import com.kodality.medipost.MedipostConfig;
import com.kodality.medipost.client.MedipostClient;
import io.micronaut.context.annotation.Bean;
import io.micronaut.context.annotation.Factory;
import io.micronaut.context.annotation.Requires;
import io.micronaut.context.annotation.Value;

@Factory
@Requires(property = "medipost.user")
public class MedipostClientFactory {
  @Value("${medipost.user}")
  private String user;

  @Value("${medipost.password}")
  private String password;

  @Value("${medipost.url}")
  private String url;

  @Bean
  public MedipostClient createMedipostClient() {
    MedipostConfig conf = MedipostConfig.builder().user(user).password(password).url(url).build();
    return new MedipostClient(conf);
  }
}