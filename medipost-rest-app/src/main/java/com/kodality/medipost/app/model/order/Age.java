package com.kodality.medipost.app.model.order;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class Age {
  private Integer days;
  private Integer months;
  private Integer years;
}
