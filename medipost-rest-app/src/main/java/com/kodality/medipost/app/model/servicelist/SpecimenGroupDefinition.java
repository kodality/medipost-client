package com.kodality.medipost.app.model.servicelist;

import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class SpecimenGroupDefinition {
  private boolean preferred;
  private List<SpecimenDefinition> specimenDefinitions;
}
