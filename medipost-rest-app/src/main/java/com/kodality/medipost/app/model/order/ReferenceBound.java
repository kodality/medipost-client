package com.kodality.medipost.app.model.order;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class ReferenceBound {
  private String value;
  private boolean included;
}
