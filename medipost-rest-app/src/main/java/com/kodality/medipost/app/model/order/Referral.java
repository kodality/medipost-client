package com.kodality.medipost.app.model.order;

import com.kodality.medipost.app.model.Coding;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class Referral {
  private Coding identifier;
  private Coding type;
}
