package com.kodality.medipost.app.model.servicelist;

import com.kodality.medipost.app.model.MedipostMessage;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class ServiceListMessage extends MedipostMessage {
  public List<ServiceList> serviceLists;
}