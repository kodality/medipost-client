package com.kodality.medipost.app.model.order;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class Price {
  private String code;
  private String multiplier;
  private String coefficient;
  private String price;
}
