package com.kodality.medipost.app.model.servicelist;

import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class InputRequirement {
  private Integer inputParameterDefinitionSequenceNo;
  private boolean required;
  private InputParameterEnterer enterer;
  private List<String> possibleResultCodes; // Reference to code of InputParameterDefinition.possibleResults entry.
}