package com.kodality.medipost.app.model.order;

import com.kodality.medipost.app.model.MedipostMessage;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class OrderMessage extends MedipostMessage {
  private Order order;
}