package com.kodality.medipost.app.model.ack;

import com.kodality.medipost.app.model.MedipostMessage;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class AckMessage extends MedipostMessage {
  private Ack ack;
}