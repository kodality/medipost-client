package com.kodality.medipost.app.mapper.to;

import com.kodality.medipost.app.model.Coding;
import com.kodality.medipost.app.model.order.ResultType;
import com.kodality.medipost.app.model.servicelist.AdditionalInfoDefinition;
import com.kodality.medipost.app.model.servicelist.InputParameterDefinition;
import com.kodality.medipost.app.model.servicelist.InputParameterEnterer;
import com.kodality.medipost.app.model.servicelist.InputRequirement;
import com.kodality.medipost.app.model.servicelist.ObservationDefinition;
import com.kodality.medipost.app.model.servicelist.ObservationGroupDefinition;
import com.kodality.medipost.app.model.servicelist.ServiceList;
import com.kodality.medipost.app.model.servicelist.SpecimenDefinition;
import com.kodality.medipost.app.model.servicelist.SpecimenGroupDefinition;
import com.kodality.medipost.model.common.YesNo;
import com.kodality.medipost.model.observation.AdditionalInfo;
import com.kodality.medipost.model.observation.BodySite;
import com.kodality.medipost.model.observation.DefinitionSpecimen;
import com.kodality.medipost.model.observation.DefinitionSpecimenGroup;
import com.kodality.medipost.model.observation.EntererType;
import com.kodality.medipost.model.observation.InputParamRelation;
import com.kodality.medipost.model.observation.InputParameter;
import com.kodality.medipost.model.observation.InputParameter.InputParameterResultType;
import com.kodality.medipost.model.observation.ObservationDef;
import com.kodality.medipost.model.observation.ObservationDefElement;
import com.kodality.medipost.model.observation.ObservationDefGroup;
import com.kodality.medipost.model.observation.PossibleInputValue;
import com.kodality.medipost.model.observation.SpecimenBodySite;
import com.kodality.medipost.model.servicelist.Performer;
import com.kodality.medipost.model.servicelist.ServiceListMessage;
import com.kodality.medipost.model.servicelist.Services;
import io.micronaut.core.util.CollectionUtils;
import jakarta.inject.Singleton;
import java.util.LinkedList;
import java.util.List;

@Singleton
public class ToMedipostServiceListMapper extends ToMedipostMapper {
  public ServiceListMessage map(com.kodality.medipost.app.model.servicelist.ServiceListMessage app) {
    ServiceListMessage msg = new ServiceListMessage();

    msg.setHeader(mapHeader(app));

    Services services = new Services();
    services.setPerformers(map(app.getServiceLists(), this::mapServiceList));
    msg.setServices(services);

    return msg;
  }

  private Performer mapServiceList(ServiceList app) {
    Performer p = new Performer();

    p.setOrganization(mapOrganization(app.getPerformer(), null));
    p.setInputParameters(map(app.getInputParameters(), this::mapInputParameterDefinition));
    p.setDefinitionGroups(map(app.getObservationGroups(), this::mapObservationGroupDefinition));

    return p;
  }

  private ObservationDefGroup  mapObservationGroupDefinition(ObservationGroupDefinition app) {
    ObservationDefGroup group = new ObservationDefGroup();

    group.setDefinitionGroupId(app.getCode() == null ? null : mkField(app.getCode().getValue()));
    group.setDefinitionGroupName(app.getCode() == null ? null : mkField(app.getCode().getName()));
    group.setAdditionalInfo(map(app.getAdditionalInfo(), this::mapAdditionalInfo));
    group.setDefinitions(map(app.getObservations(), this::mapObservationDefinition));
    group.setPrices(map(app.getPrices(), this::mapPrice));
    group.setSequenceNumber(mkField(app.getSequenceNo()));

    return group;
  }

  private ObservationDef mapObservationDefinition(ObservationDefinition app) {
    ObservationDef def = new ObservationDef();
    ObservationDefElement el = new ObservationDefElement();
    def.setElement(el);
    def.setDefinitionSpecimenGroups(map(app.getSpecimenGroupDefinitions(), this::mapSpecimenGroupDefinition));
    def.setOrderable(app.isOrderable() ? YesNo.YES : YesNo.NO);

    el.setDefinitionIdOID(app.getCode() == null ? null : mkField(app.getCode().getSystem()));
    el.setDefinitionId(app.getCode() == null ? null : mkField(app.getCode().getValue()));
    el.setTcode(app.getCode() == null ? null : mkField(app.getCode().getTAbbreviation()));
    el.setUsedName(app.getCode() == null ? null : mkField(app.getCode().getUsableName()));
    el.setNameUsedInLab(app.getCode() == null ? null : mkField(app.getCode().getName()));
    el.setMeasurementUnit(mkField(app.getResultUnit()));
    el.setSequence(app.getSequenceNo() == null ? null : mkField("" + app.getSequenceNo()));
    el.setInputParamRelations(map(app.getInputRequirements(), this::mapInputRequirement));
    el.setSpecimenDefSequenceNr(mkField(app.getSpecimenDefinitionSequenceNo()));

    return def;
  }

  private DefinitionSpecimenGroup mapSpecimenGroupDefinition(SpecimenGroupDefinition app) {
    DefinitionSpecimenGroup group = new DefinitionSpecimenGroup();

    group.setPreferred(app.isPreferred() ? YesNo.YES : YesNo.NO);
    group.setDefinitionSpecimens(map(app.getSpecimenDefinitions(), this::mapSpecimenDefinition));

    return group;
  }

  private DefinitionSpecimen mapSpecimenDefinition(SpecimenDefinition app) {
    DefinitionSpecimen s = new DefinitionSpecimen();

    s.setSpecimenTypeOID(app.getCode() == null ? null : mkField(app.getCode().getSystem()));
    s.setSpecimenType(app.getCode() == null ? null : mkField(app.getCode().getValue()));
    s.setSpecimenName(app.getCode() == null ? null : mkField(app.getCode().getName()));
    s.setContainerProperty(mkField(app.getContainerProperty()));
    s.setSequenceNumber(mkField(app.getSequenceNo()));
    s.setContainers(map(app.getContainers(), this::mapContainer));
    s.setSpecimenBodySite(mapBodySites(app));

    return s;
  }

  private SpecimenBodySite mapBodySites(SpecimenDefinition app) {
    if (CollectionUtils.isEmpty(app.getBodySites())) {
      return null;
    }

    SpecimenBodySite bs = new SpecimenBodySite();
    bs.setRequired(app.getBodySiteRequired() != null && app.getBodySiteRequired() ? YesNo.YES : YesNo.NO);
    bs.setBodySites(map(app.getBodySites(), this::mapBodySite));

    return bs;
  }

  private BodySite mapBodySite(Coding app) {
    BodySite bs = new BodySite();

    bs.setBodySiteOID(app.getSystem());
    bs.setCode(app.getValue());
    bs.setName(app.getName());

    return bs;
  }


  private InputParamRelation mapInputRequirement(InputRequirement app) {
    InputParamRelation i = new InputParamRelation();

    i.setInputParamSeqNr(app.getInputParameterDefinitionSequenceNo());
    i.setRequired(app.isRequired() ? YesNo.YES : YesNo.NO);
    i.setEnterer(mapInputParameterEnterer(app.getEnterer()));
    i.setPossibleValueIds(mkField(app.getPossibleResultCodes()));

    return i;
  }

  private EntererType mapInputParameterEnterer(InputParameterEnterer app) {
    if (app == null) {
      return null;
    }

    return switch (app) {
      case order_placer -> EntererType.AT_ORDERING;
      case specimen_collector -> EntererType.AT_COLLECTION;
    };
  }

  private AdditionalInfo mapAdditionalInfo(AdditionalInfoDefinition app) {
    AdditionalInfo i = new AdditionalInfo();

    i.setId(app.getCode() == null ? null : mkField(app.getCode().getValue()));
    i.setName(app.getCode() == null ? null : mkField(app.getCode().getName()));
    i.setValue(mkField(app.getPossibleValues()));

    return i;
  }

  private InputParameter mapInputParameterDefinition(InputParameterDefinition app) {
    InputParameter p = new InputParameter();

    p.setObservationIdOID(app.getCode() == null ? null : mkField(app.getCode().getSystem()));
    p.setObservationId(app.getCode() == null ? null : mkField(app.getCode().getValue()));
    p.setTcode(app.getCode() == null ? null : mkField(app.getCode().getTAbbreviation()));
    p.setUsedName(app.getCode() == null ? null : mkField(app.getCode().getUsableName()));
    p.setNameUsedInLab(app.getCode() == null ? null : mkField(app.getCode().getName()));
    p.setDescription(app.getCode() == null ? null : mkField(app.getCode().getDescription()));
    p.setResultType(mapResultType(app.getResultType()));
    p.setMeasurementUnit(mkField(app.getResultUnit()));
    p.setFormat(app.getFormat());
    p.setCodeSystemName(app.getCodeSystemName());
    p.setCodeSystemOID(app.getCodeSystemOID());
    p.setCodeSystemUrl(app.getCodeSystemUrl());
    p.setPossibleInputValues(mapPossibleResults(app.getPossibleResults()));
    p.setSequenceNr(mkField(app.getSequenceNo()));

    return p;
  }

  private List<PossibleInputValue> mapPossibleResults(List<Coding> app) {
    if (CollectionUtils.isEmpty(app)) {
      return null;
    }
    List<PossibleInputValue> result = new LinkedList<PossibleInputValue>();

    int sequenceNo = 0;
    for (Coding c : app) {
      PossibleInputValue v = new PossibleInputValue();
      v.setId(mkField(c.getValue()));
      v.setValue(mkField(c.getName()));
      v.setSequenceNumber(mkField(sequenceNo++));

      result.add(v);
    }

    return result;
  }

  private String mapResultType(ResultType resultType) {
    if (resultType == null) {
      return null;
    }

    return switch (resultType) {
      case quantity -> InputParameterResultType.NUMBER;
      case narrative -> InputParameterResultType.FREE_TEXT;
      case coded -> InputParameterResultType.ENCODED;
      case timepoint -> InputParameterResultType.TIMESTAMP;
    };
  }
}
