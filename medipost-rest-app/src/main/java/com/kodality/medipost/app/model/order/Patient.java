package com.kodality.medipost.app.model.order;

import com.kodality.medipost.app.model.Coding;
import java.time.LocalDate;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class Patient {
  private Coding identifier;
  private String lastName;
  private String firstName;
  private LocalDate birthDate;
  private AdministrativeGender gender;
  private Age age;
}
