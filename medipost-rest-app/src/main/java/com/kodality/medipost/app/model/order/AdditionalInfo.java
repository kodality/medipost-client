package com.kodality.medipost.app.model.order;

import com.kodality.medipost.app.model.Coding;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class AdditionalInfo {
  private Coding code;
  private List<String> values;
}