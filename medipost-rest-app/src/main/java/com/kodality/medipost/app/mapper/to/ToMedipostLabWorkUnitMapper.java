package com.kodality.medipost.app.mapper.to;

import com.kodality.medipost.app.model.Coding;
import com.kodality.medipost.app.model.order.AdministrativeGender;
import com.kodality.medipost.app.model.order.Annotation;
import com.kodality.medipost.app.model.order.LabWorkUnit;
import com.kodality.medipost.app.model.order.Observation;
import com.kodality.medipost.app.model.order.ObservationGroup;
import com.kodality.medipost.app.model.order.Practitioner;
import com.kodality.medipost.app.model.order.ReferenceRange;
import com.kodality.medipost.model.Field;
import com.kodality.medipost.model.common.Age;
import com.kodality.medipost.model.common.NormLower;
import com.kodality.medipost.model.common.NormUpper;
import com.kodality.medipost.model.common.Organization;
import com.kodality.medipost.model.common.Organization.OrganizationType;
import com.kodality.medipost.model.common.Patient;
import com.kodality.medipost.model.common.Patient.PatientSex;
import com.kodality.medipost.model.common.Personal;
import com.kodality.medipost.model.common.Personal.PersonType;
import com.kodality.medipost.model.common.ResultValue;
import com.kodality.medipost.model.common.YesNo;
import com.kodality.medipost.model.observation.AdditionalInfo;
import com.kodality.medipost.model.observation.ObservationDef;
import com.kodality.medipost.model.observation.ObservationDefElement;
import com.kodality.medipost.model.observation.ObservationDefGroup;
import com.kodality.medipost.model.observation.ObservationResult;
import com.kodality.medipost.model.observation.ObservationStatus;
import com.kodality.medipost.model.observation.Specimen;
import com.kodality.medipost.model.observation.SystematicInterpretation;
import com.kodality.medipost.model.order.ReferralDiagnosis;
import io.micronaut.core.util.CollectionUtils;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class ToMedipostLabWorkUnitMapper extends ToMedipostMapper {

  protected List<Personal> mapPractitioners(LabWorkUnit app) {
    List<Personal> practitioners = new LinkedList<>();

    if (app.getPlacerPractitioner() != null) {
      practitioners.add(mapPractitioner(app.getPlacerPractitioner(), PersonType.CUSTOMER));
    }

    if (!CollectionUtils.isEmpty(app.getPerformerPractitioners())) {
      practitioners.addAll(app.getPerformerPractitioners().stream()
          .map(p -> mapPractitioner(p, PersonType.PERFORMER)).toList());
    }

    if (!CollectionUtils.isEmpty(app.getDataEntererPractitioners())) {
      practitioners.addAll(app.getDataEntererPractitioners().stream()
          .map(p -> mapPractitioner(p, PersonType.ENTERER)).toList());
    }

    return practitioners;
  }

  private Personal mapPractitioner(Practitioner app, String type) {
    Personal p = new Personal();

    p.setPersonOID(app.getIdentifier() == null ? null : mkField(app.getIdentifier().getSystem()));
    p.setCode(app.getIdentifier() == null ? null : mkField(app.getIdentifier().getValue()));
    p.setLastName(mkField(app.getLastName()));
    p.setFirstName(mkField(app.getFirstName()));
    p.setSpeciality(mkField(app.getSpeciality()));
    p.setPhone(mkField(app.getPhone()));
    p.setType(type);
    p.setSequenceNumber(app.getSequenceNo());

    return p;
  }

  protected List<Organization> mapOrganizations(LabWorkUnit app) {
    List<Organization> organizations = new LinkedList<>();

    if (app.getPlacerOrganization() != null) {
      organizations.add(mapOrganization(app.getPlacerOrganization(), OrganizationType.CUSTOMER));
    }

    if (!CollectionUtils.isEmpty(app.getPerformerOrganizations())) {
      organizations.addAll(app.getPerformerOrganizations().stream()
          .map(o -> mapOrganization(o, OrganizationType.PERFORMER)).toList());
    }

    return organizations;
  }

  private Age mapAge(com.kodality.medipost.app.model.order.Age app) {
    if (app == null) {
      return null;
    }

    Age age = new Age();
    age.setDays(app.getDays() == null ? null : mkField(app.getDays()));
    age.setMonths(app.getMonths() == null ? null : mkField(app.getMonths()));
    age.setYears(app.getYears() == null ? null : mkField(app.getYears()));

    return age;
  }

  private String mapGender(AdministrativeGender app) {
    if (app == null) {
      return null;
    }

    return switch (app) {
      case male -> PatientSex.MALE;
      case female -> PatientSex.FEMALE;
      case unknown -> PatientSex.UNKOWN;
    };
  }

  protected ObservationDefGroup mapObservationGroup(ObservationGroup app) {
    ObservationDefGroup group = new ObservationDefGroup();

    group.setDefinitionGroupId(app.getCode() == null ? null : mkField(app.getCode().getValue()));
    group.setDefinitionGroupName(app.getCode() == null ? null : mkField(app.getCode().getName()));
    group.setAdditionalInfo(map(app.getAdditionalInfo(), this::mapAdditionalInfo));
    group.setDefinitions(map(app.getObservations(), this::mapObservation));
    group.setPrices(map(app.getPrices(), this::mapPrice));

    return group;
  }

  private AdditionalInfo mapAdditionalInfo(com.kodality.medipost.app.model.order.AdditionalInfo app) {
    AdditionalInfo ai = new AdditionalInfo();

    ai.setId(app.getCode() == null ? null : mkField(app.getCode().getValue()));
    ai.setName(app.getCode() == null ? null : mkField(app.getCode().getName()));
    ai.setValue(CollectionUtils.isEmpty(app.getValues()) ? null : app.getValues().stream().map(this::mkField).toList());

    return ai;
  }

  private ObservationDef mapObservation(Observation app) {
    ObservationDef o = new ObservationDef();
    o.setElement(mapObservationElement(app));
    o.setPerformerOrganizationSeqNr(mkField(app.getPerformerOrganizationSequenceNo()));
    o.setPerformerPersonSeqNr(mkIntField(app.getPerformerPractitionerSequenceNo()));
    o.setSpecimenSequenceNumbers(mkIntField(app.getSpecimenSequenceNo()));
    return o;
  }

  private ObservationDefElement mapObservationElement(Observation app) {
    ObservationDefElement el = new ObservationDefElement();

    el.setDefinitionIdOID(app.getCode() == null ? null : mkField(app.getCode().getSystem()));
    el.setDefinitionId(app.getCode() == null ? null : mkField(app.getCode().getValue()));
    el.setTcode(app.getCode() == null ? null : mkField(app.getCode().getTAbbreviation()));
    el.setUsedName(app.getCode() == null ? null : mkField(app.getCode().getUsableName()));
    el.setNameUsedInLab(app.getCode() == null ? null : mkField(app.getCode().getName()));
    el.setCustomerObservationId(mkField(app.getPlacerObservationId()));
    el.setLabDefinitionId(mkField(app.getPerformerObservationId()));
    el.setStatus(mapObservationStatus(app.getStatus()));
    el.setPrices(map(app.getPrices(), this::mapPrice));
    el.setElements(map(app.getComponents(), this::mapObservationElement));
    el.setSpecimenSequenceNumbers(mkIntField(app.getSpecimenSequenceNo()));
    el.setInputParametersIds(mkField(app.getInputParameterIds()));
    el.setReplacedResultId(mkField(app.getReplacedObservationId()));
    el.setMeasurementUnit(mkField(app.getResultUnit()));
    el.setSequence(app.getSequenceNo() == null ? null : mkField("" + app.getSequenceNo()));
    el.setComment(mapNotes(app.getNotes()));
    el.setEnterer(app.getAddedByPerformer() != null && app.getAddedByPerformer() ? mkField("teostaja") : null);
    el.setPerformerOrganizationSeqNr(mkField(app.getPerformerOrganizationSequenceNo()));
    el.setPerformerPersonSeqNr(mkIntField(app.getPerformerPractitionerSequenceNo()));
    el.setRelatedObservationResultId(mkField(app.getParentObservationId()));
    el.setExecutionStart(mkField(app.getPerformanceStartTime()));
    el.setExecutionEnd(mkField(app.getPerformanceEndTime()));

    if (app.getResult() != null || app.getCodedResult() != null) {
      ObservationResult res = new ObservationResult();
      res.setResultValue(mapResultValue(app));
      res.setResultInterpretation(mkField(app.getInterpretation()));
      res.setSystematicInterpretation(mapInterpretationCode(app.getInterpretationCode()));
      res.setResultCodeOID(app.getCodedResult() == null ? null : mkField(app.getCodedResult().getSystem()));
      res.setResultCode(app.getCodedResult() == null ? null : mkField(app.getCodedResult().getValue()));
      res.setResultDate(mkField(app.getVerificationTime()));
      res.setResultEntererSequenceNumber(mkField(app.getResultEntererPractitionerSequenceNo()));
      res.setNormLower(mapLow(app.getReferenceRange()));
      res.setNormUpper(mapHigh(app.getReferenceRange()));
      res.setNormComment(app.getReferenceRange() == null ? null : mkField(app.getReferenceRange().getText()));
      res.setNormStatus(mkField(mapStatus(app.getReferenceRange())));
      res.setSpecimenSequenceNumber(mkIntField(app.getSpecimenSequenceNo()));
    }

    return el;
  }

  private SystematicInterpretation mapInterpretationCode(Coding app) {
    if (app == null) {
      return null;
    }

    SystematicInterpretation i = new SystematicInterpretation();

    i.setOid(app.getSystem());
    i.setCode(app.getValue());
    i.setName(app.getName());

    return i;
  }

  protected String mapStatus(ReferenceRange app) {
    if (app == null || app.getInterpretation() == null) {
      return null;
    }

    return switch (app.getInterpretation()) {
      case normal -> "0";
      case warning -> "1";
      case alert -> "2";
    };
  }

  protected NormUpper mapHigh(ReferenceRange app) {
    if (app == null || app.getHigh() == null) {
      return null;
    }
    NormUpper high = new NormUpper();
    high.setContent(app.getHigh().getValue());
    high.setInclude(app.getHigh().isIncluded() ? YesNo.YES : YesNo.NO);
    return high;
  }

  protected NormLower mapLow(ReferenceRange app) {
    if (app == null || app.getLow() == null) {
      return null;
    }
    NormLower low = new NormLower();
    low.setContent(app.getLow().getValue());
    low.setInclude(app.getLow().isIncluded() ? YesNo.YES : YesNo.NO);
    return low;
  }

  protected ResultValue mapResultValue(Observation app) {
    if (app.getResult() != null) {
      ResultValue v = new ResultValue();
      v.setContent(app.getResult());
      return v;
    } else if (app.getCodedResult() != null) {
      ResultValue v = new ResultValue();
      v.setContent(app.getCodedResult().getName());
      return v;
    } else {
      return null;
    }
  }

  private ObservationStatus mapObservationStatus(com.kodality.medipost.app.model.order.ObservationStatus app) {
    if (app == null) {
      return null;
    }

    return switch (app) {
      case ordered -> ObservationStatus.QUEUED;
      case on_hold -> ObservationStatus.ON_HOLD;
      case in_progress -> ObservationStatus.PROCESSING;
      case aborted -> ObservationStatus.REJECTED;
      case completed -> ObservationStatus.COMPLETED;
      case nullified -> ObservationStatus.CANCELLED;
    };
  }

  protected Field mapNotes(List<Annotation> app) {
    return CollectionUtils.isEmpty(app)
      ? null : mkField(app.stream().map(Annotation::getText).collect(Collectors.joining(", ")));
  }

  protected Patient mapPatient(com.kodality.medipost.app.model.order.Patient app) {
    if (app == null) {
      return null;
    }

    Patient patient = new Patient();

    patient.setPersonalCodeOID(app.getIdentifier() == null ? null : mkField(app.getIdentifier().getSystem()));
    patient.setPersonalCode(app.getIdentifier() == null ? null : mkField(app.getIdentifier().getValue()));
    patient.setLastName(mkField(app.getLastName()));
    patient.setFirstName(mkField(app.getFirstName()));
    patient.setBirthDate(mkField(app.getBirthDate()));
    patient.setSexOID(mkField("1.3.6.1.4.1.28284.6.2.3.16.2"));
    patient.setSex(mkField(mapGender(app.getGender())));
    patient.setAge(mapAge(app.getAge()));

    return patient;
  }

  protected Specimen mapSpecimen(com.kodality.medipost.app.model.order.Specimen app) {
    Specimen s = new Specimen();

    s.setContainerIdOID(app.getIdentifier() == null ? null : mkField(app.getIdentifier().getSystem()));
    s.setContainerId(app.getIdentifier() == null ? null : mkField(app.getIdentifier().getValue()));
    s.setSpecimenTypeOID(app.getSpecimenType() == null ? null : mkField(app.getSpecimenType().getSystem()));
    s.setSpecimenType(app.getSpecimenType() == null ? null : mkField(app.getSpecimenType().getValue()));
    s.setSpecimenName(app.getSpecimenType() == null ? null : mkField(app.getSpecimenType().getName()));
    s.setBarcode(mkField(app.getBarcode()));
    s.setSequenceNumber(mkField(app.getSequenceNo()));
    s.setTakenDate(mkField(app.getCollectedTime()));
    s.setArrivalDate(mkField(app.getRegisteredInLabTime()));
    s.setComment(mapNotes(app.getNotes()));
    s.setAdequateOID(app.getAdequacy() == null ? null : mkField(app.getAdequacy().getSystem()));
    s.setAdequate(app.getAdequacy() == null ? null : mkField(app.getAdequacy().getValue()));
    s.setBodySiteOID(app.getBodySite() == null ? null : mkField(app.getBodySite().getSystem()));
    s.setBodySite(app.getBodySite() == null ? null : mkField(app.getBodySite().getValue()));
    s.setCustomerSpecimenId(mkField(app.getPlacerSpecimenId()));
    s.setContainer(mapContainer(app.getContainer()));

    return s;
  }

  protected ReferralDiagnosis mapReferralDiagnosis(Coding app) {
    ReferralDiagnosis d = new ReferralDiagnosis();

    d.setCode(mkField(app.getValue()));
    d.setName(mkField(app.getName()));
    d.setDescription(mkField(app.getDescription()));

    return d;
  }
}