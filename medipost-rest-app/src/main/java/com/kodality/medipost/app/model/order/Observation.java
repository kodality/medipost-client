package com.kodality.medipost.app.model.order;

import com.kodality.medipost.app.model.Coding;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class Observation {
  private Coding code;
  private String placerObservationId;
  private String performerObservationId; // Applicable only in result message.
  private Integer sequenceNo;
  private ObservationStatus status;
  private List<Integer> specimenSequenceNo;
  private List<Price> prices; // Applicable only in result message.
  private List<Annotation> notes;
  private Boolean addedByPerformer;

  private String replacedObservationId; // Reference by Observation.placerId.
  private String parentObservationId; // Reference by Observation.placerId. Applicable only in result message.
  private List<String> inputParameterIds; // Reference by Observation.placerId. Not applicable on input parameters.
  private List<Observation> components;

  private Coding codedResult; // When result type is coded.
  private String result; // When result type is not coded.
  private String resultUnit;
  private LocalDateTime verificationTime;
  private LocalDateTime performanceStartTime;
  private LocalDateTime performanceEndTime;
  private List<Integer> performerPractitionerSequenceNo;
  private Integer performerOrganizationSequenceNo;
  private Integer resultEntererPractitionerSequenceNo;
  private String interpretation;
  private Coding interpretationCode;
  private ReferenceRange referenceRange;

  public Observation addNote(String text) {
    if (text == null || text.isBlank()) {
      return this;
    }
    if (notes == null) {
      notes = new LinkedList<>();
    }
    notes.add(new Annotation().setText(text));
    return this;
  }

  public Observation addComponent(Observation component) {
    if (components == null) {
      components = new LinkedList<>();
    }
    components.add(component);
    return this;
  }
}