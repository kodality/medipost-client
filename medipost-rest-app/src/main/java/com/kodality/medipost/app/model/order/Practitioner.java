package com.kodality.medipost.app.model.order;

import com.kodality.medipost.app.model.Coding;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class Practitioner {
  private Coding identifier;
  private String lastName;
  private String firstName;
  private String speciality;
  private String phone;
  private Integer sequenceNo;
}