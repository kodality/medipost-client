package com.kodality.medipost.app.mapper.to;

import com.kodality.medipost.app.model.MessageType;
import com.kodality.medipost.app.model.ack.Ack;
import com.kodality.medipost.app.model.ack.AckCode;
import com.kodality.medipost.app.model.ack.AckMessage;
import com.kodality.medipost.model.notification.Notification;
import com.kodality.medipost.model.notification.NotificationCode;
import com.kodality.medipost.model.notification.NotificationMessage;
import com.kodality.medipost.model.notification.NotificationType;
import com.kodality.medipost.model.notification.OldPacket;
import jakarta.inject.Singleton;

@Singleton
public class ToMedipostAckMapper extends ToMedipostMapper {
  public NotificationMessage map(AckMessage app) {
    NotificationMessage msg = new NotificationMessage();

    msg.setHeader(mapHeader(app));
    msg.setNotification(mapAck(app.getAck()));

    return msg;
  }

  private Notification mapAck(Ack app) {
    Notification n = new Notification();

    n.setType(mapType(app.getInResponseOfMessageType()));
    n.setCode(mapCode(app.getCode()));
    n.setComment(mkField(app.getNote()));

    OldPacket p = new OldPacket();
    p.setOldPacketId(mkField(app.getInResponseOfMessageId()));
    p.setErrorPacket(mkField(app.getInResponseOfMessage()));

    n.setOldPacket(p);

    return n;
  }

  private NotificationCode mapCode(AckCode app) {
    if (app == null) {
      return null;
    }

    return switch (app) {
      case ok -> NotificationCode.OK;
      case partial_ok -> NotificationCode.PARTIAL_OK;
      case xml_schema_error -> NotificationCode.XML_SCHEMA_ERROR;
      case data_error -> NotificationCode.DATA_ERROR;
      case order_long_processing -> NotificationCode.ORDER_LONG_PROCESSING;
      case restricted_message -> NotificationCode.RESTRICTED_MESSAGE;
      case unknown_message -> NotificationCode.UNKNOWN_MESSAGE;
      case other -> NotificationCode.OTHER;
    };
  }

  private NotificationType mapType(MessageType app) {
    if (app == null) {
      return null;
    }

    return switch (app) {
      case order -> NotificationType.ORDER;
      case result -> NotificationType.ORDER_RESULT;
      default -> throw new IllegalArgumentException(app.name());
    };
  }
}
