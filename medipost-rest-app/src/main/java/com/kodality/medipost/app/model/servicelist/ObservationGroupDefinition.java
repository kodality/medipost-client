package com.kodality.medipost.app.model.servicelist;

import com.kodality.medipost.app.model.Coding;
import com.kodality.medipost.app.model.order.Price;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class ObservationGroupDefinition {
  private Coding code;
  private List<ObservationDefinition> observations;
  private List<AdditionalInfoDefinition> additionalInfo;
  private List<Price> prices;
  private Integer sequenceNo;
}