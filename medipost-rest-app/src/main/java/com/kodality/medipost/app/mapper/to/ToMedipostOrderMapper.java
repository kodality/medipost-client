package com.kodality.medipost.app.mapper.to;

import com.kodality.medipost.app.model.Coding;
import com.kodality.medipost.app.model.order.Observation;
import com.kodality.medipost.model.Field;
import com.kodality.medipost.model.common.YesNo;
import com.kodality.medipost.model.observation.InputParameter;
import com.kodality.medipost.model.observation.InputParameterResult;
import com.kodality.medipost.model.order.Confidentiality;
import com.kodality.medipost.model.order.Order;
import com.kodality.medipost.model.order.OrderMessage;
import com.kodality.medipost.model.order.Referral;
import com.kodality.medipost.model.order.SicknessCase;
import io.micronaut.core.util.CollectionUtils;
import jakarta.inject.Singleton;

@Singleton
public class ToMedipostOrderMapper extends ToMedipostLabWorkUnitMapper {
  public OrderMessage map(com.kodality.medipost.app.model.order.OrderMessage app) {
    OrderMessage msg = new OrderMessage();

    msg.setHeader(mapHeader(app));
    msg.setOrder(mapOrder(app.getOrder()));

    return msg;
  }

  private Order mapOrder(com.kodality.medipost.app.model.order.Order app) {
    Order order = new Order();

    order.setCustomerOrderId(mkField(app.getPlacerOrderId()));
    order.setOrganizations(mapOrganizations(app));
    order.setPersonal(mapPractitioners(app));
    order.setCustomerComment(mapNotes(app.getNotes()));
    order.setReferralDiagnoses(map(app.getReferralDiagnoses(), this::mapReferralDiagnosis));
    order.setPatient(mapPatient(app.getPatient()));
    order.setConfidentiality(mapConfidentiality(app.getConfidentiality()));
    order.setReferral(mapReferral(app.getReferral()));
    order.setSicknessCase(mapSicknessCase(app.getSicknessCaseId()));
    order.setCommonIdOID(app.getCommonId() == null ? null : mkField(app.getCommonId().getSystem()));
    order.setCommonId(app.getCommonId() == null ? null : mkField(app.getCommonId().getValue()));
    order.setSpecimens(map(app.getSpecimens(), this::mapSpecimen));
    order.setInputParameters(map(app.getInputParameters(), this::mapInputParameter));
    order.setObservationGroups(map(app.getObservationGroups(), this::mapObservationGroup));
    order.setCito(app.isCito() ? YesNo.YES : YesNo.NO);

    return order;
  }

  private InputParameter mapInputParameter(Observation app) {
    InputParameter p = new InputParameter();

    p.setObservationIdOID(app.getCode() == null ? null : mkField(app.getCode().getSystem()));
    p.setObservationId(app.getCode() == null ? null : mkField(app.getCode().getValue()));
    p.setTcode(app.getCode() == null ? null : mkField(app.getCode().getTAbbreviation()));
    p.setUsedName(app.getCode() == null ? null : mkField(app.getCode().getUsableName()));
    p.setNameUsedInLab(app.getCode() == null ? null : mkField(app.getCode().getName()));
    p.setDescription(app.getCode() == null ? null : mkField(app.getCode().getDescription()));
    p.setCustomerObservationId(mkField(app.getPlacerObservationId()));
    p.setSequenceNr(mkField(app.getSequenceNo()));
    p.setReplacedResultId(mkField(app.getReplacedObservationId()));
    p.setInputParameterStatus(mapInputParameterStatus(app.getStatus()));
    p.setMeasurementUnit(mkField(app.getResultUnit()));

    InputParameterResult r = new InputParameterResult();
    r.setResultValue(mapResultValue(app));
    r.setResultInterpretation(mkField(app.getInterpretation()));
    r.setResultCodeOID(app.getCodedResult() == null ? null : mkField(app.getCodedResult().getSystem()));
    r.setResultCode(app.getCodedResult() == null ? null : mkField(app.getCodedResult().getValue()));
    r.setResultDate(mkField(app.getVerificationTime()));
    r.setResultAuthorSequenceNr(CollectionUtils.isEmpty(app.getPerformerPractitionerSequenceNo())
        ? null : mkField("" + app.getPerformerPractitionerSequenceNo().iterator().next()));
    r.setNormLower(mapLow(app.getReferenceRange()));
    r.setNormUpper(mapHigh(app.getReferenceRange()));
    r.setNormComment(app.getReferenceRange() == null ? null : mkField(app.getReferenceRange().getText()));
    r.setNormStatus(mkField(mapStatus(app.getReferenceRange())));
    r.setSpecimenSequenceNumbers(mkIntField(app.getSpecimenSequenceNo()));

    return p;
  }

  private Field mapInputParameterStatus(com.kodality.medipost.app.model.order.ObservationStatus app) {
    if (app == null) {
      return null;
    }

    return switch (app) {
      case completed -> mkField("4");
      case nullified -> mkField("6");
      default -> throw new IllegalArgumentException(app.name());
    };
  }

  private SicknessCase mapSicknessCase(Coding sicknessCaseId) {
    if (sicknessCaseId == null) {
      return null;
    }

    SicknessCase sc = new SicknessCase();

    sc.setSicknessCaseOID(mkField(sicknessCaseId.getSystem()));
    sc.setSicknessCaseNumber(mkField(sicknessCaseId.getValue()));

    return sc;
  }

  private Referral mapReferral(com.kodality.medipost.app.model.order.Referral app) {
    if (app == null) {
      return null;
    }

    Referral r = new Referral();

    r.setReferralNumberOID(app.getIdentifier() == null ? null : mkField(app.getIdentifier().getSystem()));
    r.setReferralNumber(app.getIdentifier() == null ? null : mkField(app.getIdentifier().getValue()));
    r.setReferralTypeOID(app.getType() == null ? null : mkField(app.getType().getSystem()));
    r.setReferralType(app.getType() == null ? null : mkField(app.getType().getValue()));

    return r;
  }

  private Confidentiality mapConfidentiality(com.kodality.medipost.app.model.order.Confidentiality app) {
    if (app == null) {
      return null;
    }

    Confidentiality conf = new Confidentiality();

    conf.setPatientConfidentialityOID(app.getForPatient() == null ? null : mkField(app.getForPatient().getSystem()));
    conf.setPatientConfidentiality(app.getForPatient() == null ? null : mkField(app.getForPatient().getValue()));
    conf.setPractitionerConfidentialityOID(app.getForPractitioner() == null ? null : mkField(app.getForPractitioner().getSystem()));
    conf.setPractitionerConfidentiality(app.getForPractitioner() == null ? null : mkField(app.getForPractitioner().getValue()));
    conf.setRepresentativeConfidentialityOID(app.getForRepresentative() == null ? null : mkField(app.getForRepresentative().getSystem()));
    conf.setRepresentativeConfidentiality(app.getForRepresentative() == null ? null : mkField(app.getForRepresentative().getValue()));
    conf.setPatientOpenDate(mkField(app.getPatientOpenDate()));

    return conf;
  }


}
