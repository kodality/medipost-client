package com.kodality.medipost.app.model.order;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class Organization {
  private String regCode;
  private String medipostCode;
  private String name;
  private String substructureName;
  private String phone;
  private String municipalityEhakCode;
  private String address;
  private Integer sequenceNo;
}
