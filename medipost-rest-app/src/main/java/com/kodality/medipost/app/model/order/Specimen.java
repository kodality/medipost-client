package com.kodality.medipost.app.model.order;

import com.kodality.medipost.app.model.Coding;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class Specimen {
  private String placerSpecimenId;
  private Coding identifier;
  private Coding specimenType;
  private Coding container;
  private String barcode;
  private Integer sequenceNo;
  private LocalDateTime collectedTime;
  private LocalDateTime registeredInLabTime;
  private List<Annotation> notes;
  private Coding adequacy;
  private Coding bodySite;

  public Specimen addNote(String text) {
    if (text == null || text.isBlank()) {
      return this;
    }
    if (notes == null) {
      notes = new LinkedList<>();
    }
    notes.add(new Annotation().setText(text));
    return this;
  }
}