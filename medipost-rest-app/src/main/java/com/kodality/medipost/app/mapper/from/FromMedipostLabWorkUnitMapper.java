package com.kodality.medipost.app.mapper.from;

import com.kodality.medipost.app.model.Coding;
import com.kodality.medipost.app.model.order.AdditionalInfo;
import com.kodality.medipost.app.model.order.AdministrativeGender;
import com.kodality.medipost.app.model.order.Age;
import com.kodality.medipost.app.model.order.Confidentiality;
import com.kodality.medipost.app.model.order.Observation;
import com.kodality.medipost.app.model.order.ObservationGroup;
import com.kodality.medipost.app.model.order.ObservationStatus;
import com.kodality.medipost.app.model.order.Organization;
import com.kodality.medipost.app.model.order.Patient;
import com.kodality.medipost.app.model.order.Practitioner;
import com.kodality.medipost.app.model.order.ReferenceBound;
import com.kodality.medipost.app.model.order.ReferenceRange;
import com.kodality.medipost.app.model.order.ReferenceRangeInterpretation;
import com.kodality.medipost.app.model.order.Specimen;
import com.kodality.medipost.model.DateField;
import com.kodality.medipost.model.IntField;
import com.kodality.medipost.model.common.NormLower;
import com.kodality.medipost.model.common.NormUpper;
import com.kodality.medipost.model.common.Organization.OrganizationType;
import com.kodality.medipost.model.common.Patient.PatientSex;
import com.kodality.medipost.model.common.Personal;
import com.kodality.medipost.model.common.Personal.PersonType;
import com.kodality.medipost.model.common.ResultValue;
import com.kodality.medipost.model.common.YesNo;
import com.kodality.medipost.model.observation.ObservationDef;
import com.kodality.medipost.model.observation.ObservationDefElement;
import com.kodality.medipost.model.observation.ObservationDefGroup;
import com.kodality.medipost.model.observation.ObservationResult;
import com.kodality.medipost.model.observation.SystematicInterpretation;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class FromMedipostLabWorkUnitMapper extends FromMedipostMapper {

  protected LocalDate value(DateField field) {
    return field != null ? field.getContent() : null;
  }

  protected Organization mapPlacerOrganization(List<com.kodality.medipost.model.common.Organization> mp) {
    return isEmpty(mp)
        ? null
        : mp.stream()
        .filter(o -> OrganizationType.CUSTOMER.equals(o.getType()))
        .map(this::mapOrganization)
        .findFirst().orElse(null);
  }

  protected List<Organization> mapPerformerOrganizations(List<com.kodality.medipost.model.common.Organization> mp) {
    if (isEmpty(mp)) {
      return null;
    }
    List<Organization> performers = mp.stream()
        .filter(o -> OrganizationType.PERFORMER.equals(o.getType()))
        .map(this::mapOrganization)
        .toList();
    return performers.isEmpty() ? null : performers;
  }

  protected Practitioner mapPlacerPractitioner(List<com.kodality.medipost.model.common.Personal> mp) {
    return isEmpty(mp)
        ? null
        : mp.stream()
        .filter(p -> PersonType.CUSTOMER.equals(p.getType()))
        .map(this::mapPractitioner)
        .findFirst().orElse(null);
  }

  protected List<Practitioner> mapPractitioners(List<com.kodality.medipost.model.common.Personal> mp, String type) {
    if (isEmpty(mp)) {
      return null;
    }
    List<Practitioner> practitioners = mp.stream()
        .filter(p -> type.equals(p.getType()))
        .map(this::mapPractitioner)
        .toList();
    return practitioners.isEmpty() ? null : practitioners;
  }

  private Practitioner mapPractitioner(Personal mp) {
    return new Practitioner()
        .setIdentifier(cd(mp.getPersonOID(), mp.getCode()))
        .setLastName(value(mp.getLastName()))
        .setFirstName(value(mp.getFirstName()))
        .setSpeciality(value(mp.getSpeciality()))
        .setPhone(value(mp.getPhone()))
        .setSequenceNo(mp.getSequenceNumber());
  }

  protected Coding mapReferralDiagnosis(com.kodality.medipost.model.order.ReferralDiagnosis mp) {
    return new Coding()
        .setValue(value(mp.getCode()))
        .setName(value(mp.getName()))
        .setDescription(value(mp.getDescription()));
  }

  protected Patient mapPatient(com.kodality.medipost.model.common.Patient mp) {
    return new Patient()
        .setIdentifier(cd(mp.getPersonalCodeOID(), mp.getPersonalCode()))
        .setFirstName(value(mp.getFirstName()))
        .setLastName(value(mp.getLastName()))
        .setBirthDate(value(mp.getBirthDate()))
        .setGender(mapGender(value(mp.getSex())))
        .setAge(mapAge(mp.getAge()));
  }

  private Age mapAge(com.kodality.medipost.model.common.Age mp) {
    return mp == null
        ? null
        : new Age().setDays(value(mp.getDays())).setMonths(value(mp.getMonths())).setYears(value(mp.getYears()));
  }

  private AdministrativeGender mapGender(String gender) {
    if (gender == null) {
      return null;
    }

    return switch (gender) {
      case PatientSex.MALE -> AdministrativeGender.male;
      case PatientSex.FEMALE -> AdministrativeGender.female;
      case PatientSex.UNKOWN -> AdministrativeGender.unknown;
      default -> throw new IllegalArgumentException(gender);
    };
  }

  protected Confidentiality mapConfidentiality(com.kodality.medipost.model.order.Confidentiality mp) {
    return mp == null
        ? null
        : new Confidentiality()
        .setForPatient(cd(mp.getPatientConfidentialityOID(), mp.getPatientConfidentiality()))
        .setForPractitioner(cd(mp.getPractitionerConfidentialityOID(), mp.getPractitionerConfidentiality()))
        .setForRepresentative(cd(mp.getRepresentativeConfidentialityOID(), mp.getRepresentativeConfidentiality()))
        .setPatientOpenDate(value(mp.getPatientOpenDate()));
  }

  protected ObservationStatus mapStatus(String status) {
    if (isEmpty(status)) {
      return null;
    }

    return switch (status) {
      case "QUEUED" -> ObservationStatus.ordered;
      case "ON_HOLD" -> ObservationStatus.on_hold;
      case "PROCESSING" -> ObservationStatus.in_progress;
      case "COMPLETED" -> ObservationStatus.completed;
      case "REJECTED" -> ObservationStatus.aborted;
      case "CANCELLED" -> ObservationStatus.nullified;
      default -> throw new IllegalStateException(status);
    };
  }

  protected Specimen mapSpecimen(com.kodality.medipost.model.observation.Specimen mp) {
    return new Specimen()
        .setPlacerSpecimenId(value(mp.getCustomerSpecimenId()))
        .setIdentifier(cd(mp.getContainerIdOID(), mp.getContainerId()))
        .setSpecimenType(mapSpecimenType(mp))
        .setContainer(mapContainer(mp.getContainer()))
        .setBarcode(value(mp.getBarcode()))
        .setSequenceNo(value(mp.getSequenceNumber()))
        .setCollectedTime(value(mp.getTakenDate()))
        .setRegisteredInLabTime(value(mp.getArrivalDate()))
        .addNote(value(mp.getComment()))
        .setAdequacy(cd(mp.getAdequateOID(), mp.getAdequate()))
        .setBodySite(cd(mp.getBodySiteOID(), mp.getBodySite()));
  }

  private Coding mapSpecimenType(com.kodality.medipost.model.observation.Specimen mp) {
    Coding c = cd(mp.getSpecimenTypeOID(), mp.getSpecimenType());
    return c == null ? null : c.setName(value(mp.getSpecimenName()));
  }

  protected ObservationGroup mapObservationGroup(ObservationDefGroup mp) {
    return new ObservationGroup()
        .setCode(new Coding().setValue(value(mp.getDefinitionGroupId())).setName(value(mp.getDefinitionGroupName())))
        .setObservations(mp.getDefinitions() == null
            ? null : mp.getDefinitions().stream().filter(d -> d.getElement() != null).map(this::mapObservationDef).toList())
        .setPrices(map(mp.getPrices(), this::mapPrice))
        .setAdditionalInfo(map(mp.getAdditionalInfo(), this::mapAdditionalInfo));
  }

  private AdditionalInfo mapAdditionalInfo(com.kodality.medipost.model.observation.AdditionalInfo mp) {
    return new AdditionalInfo()
        .setCode(new Coding().setValue(value(mp.getId())).setName(value(mp.getName())))
        .setValues(values(mp.getValue()));
  }

  private Observation mapObservationDef(ObservationDef mp) {
    return mapObservationElement(mp.getElement(), mp, true);
  }

  private Observation mapObservationElement(ObservationDefElement mp, ObservationDef mpDef, boolean topmost) {
    List<Integer> performerPr = intValues(mp.getPerformerPersonSeqNr());
    Integer performerOrg = value(mp.getPerformerOrganizationSeqNr());
    LocalDateTime performanceStart = value(mp.getExecutionStart());
    LocalDateTime performanceEnd = value(mp.getExecutionEnd());
    List<Integer> specimenSeqNo = intValues(mp.getSpecimenSequenceNumbers());
    String parentId = value(mp.getRelatedObservationResultId());

    Observation obs = new Observation()
        .setCode(mapObservationCode(mp))
        .setPlacerObservationId(value(mp.getCustomerObservationId()))
        .setPerformerObservationId(value(mp.getLabDefinitionId()))
        .setSequenceNo(intValue(mp.getSequence()))
        .setStatus(mp.getStatus() == null ? null : mapStatus(mp.getStatus().name()))
        .setSpecimenSequenceNo(intValues(!isEmpty(mp.getSpecimenSequenceNumbers())
            ? mp.getSpecimenSequenceNumbers() : mpDef.getSpecimenSequenceNumbers()))
        .setInputParameterIds(values(mp.getInputParametersIds()))
        .setSequenceNo(intValue(mp.getSequence()))
        .addNote(value(mp.getComment()))
        .addNote(topmost ? value(mpDef.getComment()) : null)
        .setPerformerPractitionerSequenceNo(!isEmpty(performerPr) ? performerPr : intValues(mpDef.getPerformerPersonSeqNr()))
        .setPerformerOrganizationSequenceNo(performerOrg != null ? performerOrg : value(mpDef.getPerformerOrganizationSeqNr()))
        .setPerformanceStartTime(performanceStart != null ? performanceStart : value(mpDef.getExecutionStart()))
        .setPerformanceEndTime(performanceEnd != null ? performanceEnd : value(mpDef.getExecutionEnd()))
        .setSpecimenSequenceNo(!isEmpty(specimenSeqNo) ? specimenSeqNo : intValues(mpDef.getSpecimenSequenceNumbers()))
        .setAddedByPerformer("teostaja".equals(value(mp.getEnterer())) || "teostaja".equals(value(mpDef.getEnterer())) ? true : null)
        .setParentObservationId(!topmost || parentId != null ? parentId : value(mpDef.getRelatedObservationResultId()))
        .setReplacedObservationId(value(mp.getReplacedResultId()))
        .setInputParameterIds(values(mp.getInputParametersIds()))
        .setPrices(map(mp.getPrices(), this::mapPrice))
        .setComponents(map(mp.getElements(), e -> mapObservationElement(e, mpDef, false)));

    if (mp.getResult() != null) {
      obs
          .setCodedResult(mapCodedResult(mp.getResult()))
          .setResult(mapNotCodedResult(mp.getResult()))
          .setResultUnit(value(mp.getMeasurementUnit()))
          .setVerificationTime(value(mp.getResult().getResultDate()))
          .setInterpretation(value(mp.getResult().getResultInterpretation()))
          .setInterpretationCode(mapInterpretationCode(mp.getResult().getSystematicInterpretation()))
          .setResultEntererPractitionerSequenceNo(value(mp.getResult().getResultEntererSequenceNumber()))
          .setReferenceRange(mapReferenceRange(mp.getResult()));
    }

    return obs;
  }

  private Coding mapInterpretationCode(SystematicInterpretation mp) {
    if (mp == null) {
      return null;
    }
    String system;
    if (!isEmpty(mp.getUrl())) {
      system = mp.getUrl();
    } else if (!isEmpty(mp.getOid())) {
      system = mp.getOid();
    } else {
      system = mp.getCodeSystem();
    }

    return new Coding().setSystem(system).setValue(mp.getCode()).setName(mp.getName());
  }

  private ReferenceRange mapReferenceRange(ObservationResult mp) {
    return mapReferenceRange(mp.getNormLower(), mp.getNormUpper(), value(mp.getNormComment()), value(mp.getNormStatus()));
  }

  protected ReferenceRange mapReferenceRange(NormLower low, NormUpper high, String text, String interpretation) {
    boolean lowSet = low != null && !isEmpty(low.getContent());
    boolean highSet = high != null && !isEmpty(high.getContent());
    boolean textSet = !isEmpty(text);

    if (!lowSet && !highSet && !textSet) {
      return null;
    }

    ReferenceRange rr = new ReferenceRange();

    if (lowSet) {
      rr.setLow(new ReferenceBound().setValue(low.getContent()).setIncluded(low.getInclude() == YesNo.YES));
    }

    if (highSet) {
      rr.setHigh(new ReferenceBound().setValue(high.getContent()).setIncluded(high.getInclude() == YesNo.YES));
    }

    if (textSet) {
      rr.setText(text);
    }

    rr.setInterpretation(mapReferenceRangeInterpretation(interpretation));

    return rr;
  }

  private ReferenceRangeInterpretation mapReferenceRangeInterpretation(String mp) {
    if (isEmpty(mp)) {
      return null;
    }

    return switch (mp) {
      case "0" -> ReferenceRangeInterpretation.normal;
      case "1" -> ReferenceRangeInterpretation.warning;
      case "2" -> ReferenceRangeInterpretation.alert;
      default -> throw new IllegalStateException(mp);
    };
  }

  protected List<Integer> intValues(List<IntField> mp) {
    return isEmpty(mp) ? null : mp.stream().map(this::value).toList();
  }

  private String mapNotCodedResult(ObservationResult mp) {
    return cd(mp.getResultCodeOID(), mp.getResultCode()) != null ? null : mapResultValue(mp.getResultValue());
  }

  private Coding mapCodedResult(ObservationResult mp) {
    Coding c = cd(mp.getResultCodeOID(), mp.getResultCode());
    return c == null ? null : c.setName(mapResultValue(mp.getResultValue()));
  }

  protected String mapResultValue(ResultValue mp) {
    if (mp == null) {
      return null;
    }
    String value = mp.getContent();
    if (isEmpty(value)) {
      return null;
    }
    if ("YLES".equals(mp.getCondition())) {
      value = ">" + value;
    } else if ("ALLA".equals(mp.getCondition())) {
      value = "<" + value;
    }

    if (!isEmpty(mp.getQuotient())) {
      value = mp.getQuotient() + ":" + value;
    }

    return value;
  }
}