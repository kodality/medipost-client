package com.kodality.medipost.app.model.order;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class ReferenceRange {
  private ReferenceBound low;
  private ReferenceBound high;
  private String text;
  private ReferenceRangeInterpretation interpretation;
}
