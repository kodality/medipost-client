package com.kodality.medipost.app.mapper.from;

import com.kodality.medipost.app.model.ack.Ack;
import com.kodality.medipost.app.model.ack.AckCode;
import com.kodality.medipost.app.model.ack.AckMessage;
import com.kodality.medipost.app.model.MessageType;
import com.kodality.medipost.model.notification.Notification;
import com.kodality.medipost.model.notification.NotificationCode;
import com.kodality.medipost.model.notification.NotificationMessage;
import com.kodality.medipost.model.notification.NotificationType;
import jakarta.inject.Singleton;

@Singleton
public class FromMedipostAckMapper extends FromMedipostMapper {
  public AckMessage map(NotificationMessage mp) {
    return mapHeader(mp.getHeader(), new AckMessage()).setAck(mapAck(mp.getNotification()));
  }

  private Ack mapAck(Notification mp) {
    return new Ack()
        .setInResponseOfMessageType(mapType(mp.getType()))
        .setCode(mapCode(mp.getCode()))
        .setNote(value(mp.getComment()))
        .setInResponseOfMessageId(value(mp.getOldPacket().getOldPacketId()))
        .setInResponseOfMessage(value(mp.getOldPacket().getErrorPacket()));
  }

  private AckCode mapCode(NotificationCode code) {
    if (code == null) {
      return null;
    }

    return switch (code) {
      case OK -> AckCode.ok;
      case PARTIAL_OK -> AckCode.partial_ok;
      case XML_SCHEMA_ERROR -> AckCode.xml_schema_error;
      case DATA_ERROR -> AckCode.data_error;
      case ORDER_LONG_PROCESSING -> AckCode.order_long_processing;
      case RESTRICTED_MESSAGE -> AckCode.restricted_message;
      case UNKNOWN_MESSAGE -> AckCode.unknown_message;
      case OTHER -> AckCode.other;
    };
  }

  private MessageType mapType(NotificationType type) {
    if (type == null) {
      return null;
    }

    return switch (type) {
      case ORDER -> MessageType.order;
      case ORDER_RESULT -> MessageType.result;
      default -> throw new IllegalArgumentException(type.name());
    };
  }
}