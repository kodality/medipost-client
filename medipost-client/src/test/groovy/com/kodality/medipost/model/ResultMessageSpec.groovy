package com.kodality.medipost.model

import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.kodality.medipost.model.common.YesNo
import com.kodality.medipost.model.result.ResultMessage
import com.kodality.medipost.model.result.ResultStatus
import com.kodality.medipost.util.MapperUtil
import spock.lang.Specification

import java.nio.file.Files
import java.nio.file.Path

class ResultMessageSpec extends Specification {
  def "test deserialization"() {
    XmlMapper xmlMapper = MapperUtil.getMapper()
    def messageText = Files.readString(Path.of(ResultMessage.class.getClassLoader().getResource("VastusLOINC.xml").toURI()))
    def message = xmlMapper.readValue(messageText, ResultMessage.class)
    expect:
    message.result.organizations[0].id.content == '9000147855'
    message.result.specimens[2].barcode.content == 'ABC00001SA'
    message.result.observationGroups[0].definitions[0].element.elements[1].tcode.content == 'B-Hct'
    message.result.orderStatus == ResultStatus.PROCESSING
  }
}
