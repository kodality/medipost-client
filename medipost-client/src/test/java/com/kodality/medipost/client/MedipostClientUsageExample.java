package com.kodality.medipost.client;

import com.kodality.medipost.MedipostConfig;
import com.kodality.medipost.model.message.MessageItem;
import com.kodality.medipost.model.message.MessageList;
import com.kodality.medipost.model.order.OrderMessage;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

public class MedipostClientUsageExample {


  public static void main(String[] args) throws Exception {
    // see https://kodality.atlassian.net/wiki/spaces/HEDA/pages/371032180/Medipost

    Properties prop = new Properties();

    prop.load(new FileInputStream("config.properties"));
    String password = prop.getProperty("medipost.password");
    String user = prop.getProperty("medipost.user");

    MedipostConfig config =
        MedipostConfig.builder().disableHostnameVerification(true)
            .url("https://veeb.medisoft.ee:7443/Medipost/MedipostServlet")
            .user(user)
            .password(password).build();
    MedipostClient client = new MedipostClient(config);

    String message = Files.readString(Path.of(MedipostClient.class.getClassLoader().getResource("TellimusLOINC_valid.xml").toURI()));
    OrderMessage orderMessage = client.getXmlMapper().readValue(message, OrderMessage.class);

    // send order
    Response response = client.sendOrder(orderMessage, user);

    System.out.println(response);

    assert response.getCode() == 0;
    // receive order
    MessageList messageList = client.getPrivateMessageList();
    assert messageList.getMessages() != null && messageList.getMessages().size() > 0;

    System.out.println(messageList);

    MessageItem item = messageList.getMessages().get(messageList.getMessages().size() - 1);
    String messageId = item.getId();

    OrderMessage orderMessage1 = client.getOrder(messageId);
    System.out.println(orderMessage.getHeader());
    System.out.println(orderMessage1.getHeader());
    assert orderMessage.getHeader().getSenderMessageId().getContent().equals(orderMessage1.getHeader().getSenderMessageId().getContent());
    // delete order msg

    Response response1 = client.deletePrivateMessage(messageId);
    System.out.println(response1);

    assert response1.getCode() == 0;

  }
}
