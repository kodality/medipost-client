package com.kodality.medipost.client;

import com.kodality.medipost.MedipostConfig;
import com.kodality.medipost.model.message.MessageList;
import com.kodality.medipost.model.order.Order;
import com.kodality.medipost.model.servicelist.ServiceListMessage;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

public class MedipostClientServiceListExample {
  public static void main(String[] args) throws IOException, URISyntaxException {
    Properties prop = new Properties();

    prop.load(new FileInputStream("config.properties"));
    String password = prop.getProperty("medipost.password");
    String user = prop.getProperty("medipost.user");

    MedipostConfig config =
        MedipostConfig.builder().disableHostnameVerification(true)
            .url("https://veeb.medisoft.ee:7443/Medipost/MedipostServlet")
            .user(user)
            .password(password).build();
    MedipostClient client = new MedipostClient(config);

    MessageList messageList = client.getPublicServicesMessageList("kodalheda");
    System.out.println(messageList);
    System.out.println();

    messageList.getMessages().stream().forEach(m -> {
      System.out.println("-----------");
      System.out.println(m.getId());
      System.out.println(m.getSender());
      System.out.println(m.getDate());
      System.out.println(m.getType());
    });
    ServiceListMessage serviceListMessage = client.getLatestServiceList("kodalheda").get();
    System.out.println(serviceListMessage);


    //ServiceListMessage serviceListMessage = client.getServiceList("11241634");
    //System.out.println(serviceListMessage);

    //String message = Files.readString(Path.of(MedipostClient.class.getClassLoader().getResource("TeenusedLOINC_valid.xml").toURI()));
    //ServiceListMessage serviceListMessage1 = client.getXmlMapper().readValue(message, ServiceListMessage.class);

    //System.out.println(client.sendServiceList(serviceListMessage1));
  }
}
