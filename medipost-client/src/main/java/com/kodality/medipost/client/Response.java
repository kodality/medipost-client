package com.kodality.medipost.client;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
@JacksonXmlRootElement(localName = "ANSWER")
public class Response {
  @JacksonXmlProperty(localName = "CODE")
  private int code;
  @JacksonXmlProperty(localName = "TEXT")
  private String text;

  @JsonIgnore
  public boolean isOk() {
    return ResponseCode.OK == code;
  }

  public interface ResponseCode {
    int OK = 0;
    int WRONG_CREDENTIALS = 1;
    int WRONG_PARAMETER = 2;

    int XML_IMPORT_FAILED = 25;

    // there are others
  }
}
