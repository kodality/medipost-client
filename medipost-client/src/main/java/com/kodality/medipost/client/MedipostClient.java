package com.kodality.medipost.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kodality.medipost.MedipostConfig;
import com.kodality.medipost.model.MessageType;
import com.kodality.medipost.model.message.MessageItem;
import com.kodality.medipost.model.message.MessageList;
import com.kodality.medipost.model.notification.NotificationMessage;
import com.kodality.medipost.model.order.OrderMessage;
import com.kodality.medipost.model.result.ResultMessage;
import com.kodality.medipost.model.servicelist.ServiceListMessage;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;


public class MedipostClient extends MedipostHttpClient {

  private MedipostConfig config;

  public MedipostClient() {
  }

  public MedipostClient(MedipostConfig config) {
    super(config.isDisableHostnameVerification());
    this.config = config;
  }

  public MessageList getPrivateMessageList() {
    String url = config.getUrl() + "?Action=GetPrivateMessageList" +
        "&User=" + config.getUser() +
        "&password=" + config.getPassword();

    return doGet(url, MessageList.class);
  }

  public MessageList getPublicMessageList(String user) {
    String url = config.getUrl() + "?Action=GetPublicMessageList" +
        "&User=" + config.getUser() +
        "&password=" + config.getPassword();
    if(user != null) {
      url += "&Sender=" + user;
    }
    return doGet(url, MessageList.class);
  }

  public MessageList getPublicMessageList() {
    return getPublicMessageList(null);
  }

  public MessageList getPublicServicesMessageList() {
    return getPublicServicesMessageList(null);
  }

  public MessageList getPublicServicesMessageList(String sender) {
    String url = config.getUrl() + "?Action=GetPublicMessageList&MessageType=Teenus" +
        "&User=" + config.getUser() +
        "&password=" + config.getPassword();
    if(sender != null) {
      url += "&Sender=" + sender;
    }
    return doGet(url, MessageList.class);
  }

  public Response deletePrivateMessage(String messageId) {
    String url = config.getUrl() + "?Action=DeletePrivateMessage" +
        "&User=" + config.getUser() +
        "&password=" + config.getPassword() +
        "&MessageId=" + messageId;

    return doGet(url, Response.class);
  }

  public Response deletePublicMessage(String messageId) {
    String url = config.getUrl() + "?Action=DeletePublicMessage" +
        "&User=" + config.getUser() +
        "&password=" + config.getPassword() +
        "&MessageId=" + messageId;

    return doGet(url, Response.class);
  }

  public OrderMessage getOrder(String messageId) {
    return getPrivateMessage(messageId, OrderMessage.class);
  }

  public Response sendOrder(OrderMessage order, String recipient) {
    return sendPrivateMessage(order, recipient, MessageType.Order.getCode());
  }

  public Response sendNotification(NotificationMessage notification, String recipient) {
    return sendPrivateMessage(notification, recipient, MessageType.Notification.getCode());
  }

  public NotificationMessage getNotification(String messageId) {
    return getPrivateMessage(messageId, NotificationMessage.class);
  }

  public Response sendResult(ResultMessage result, String recipient) {
    return sendPrivateMessage(result, recipient, MessageType.Result.getCode());
  }

  public ResultMessage getResult(String messageId) {
    return getPrivateMessage(messageId, ResultMessage.class);
  }

  public ServiceListMessage getServiceList(String messageId) {
    String url = config.getUrl() + "?Action=GetPublicMessage" +
        "&User=" + config.getUser() +
        "&password=" + config.getPassword() +
        "&MessageId=" + messageId;

    return doGet(url, ServiceListMessage.class);
  }

  public Optional<ServiceListMessage> getLatestServiceList(String user) {
    MessageList messageList = getPublicMessageList(user);
    List<MessageItem> items = messageList.getMessages();
    items.sort(Comparator.comparing(MessageItem::getDate));
    if(items.isEmpty()) {
      return Optional.empty();
    }
    MessageItem last = items.get(items.size() - 1);
    return Optional.of(getServiceList(last.getId()));
  }

  public Response sendServiceList(ServiceListMessage serviceListMessage) {
    return sendPublicMessage(serviceListMessage, MessageType.Services.getCode());
  }

  public Response sendPublicMessage(Object message, String type) {
    try {
      MultiPartBodyPublisher publisher = new MultiPartBodyPublisher()
          .addPart("Action", "SendPublicMessage")
          .addPart("User", config.getUser())
          .addPart("Password", config.getPassword())
          .addPart("MessageType", type)
          .addXml("Message", getXmlMapper().writeValueAsString(message));
      return doPost(config.getUrl(), publisher, Response.class);

    } catch (JsonProcessingException e) {
      throw new RuntimeException(e);
    }
  }

  private Response sendPrivateMessage(Object message, String recipient, String type) {

    try {
      MultiPartBodyPublisher publisher = new MultiPartBodyPublisher()
          .addPart("Action", "SendPrivateMessage")
          .addPart("User", config.getUser())
          .addPart("Password", config.getPassword())
          .addPart("MessageType", type)
          .addPart("Receiver", recipient)
          .addXml("Message", getXmlMapper().writeValueAsString(message));
      return doPost(config.getUrl(), publisher, Response.class);

    } catch (JsonProcessingException e) {
      throw new RuntimeException(e);
    }
  }

  private <T> T getPrivateMessage(String messageId, Class<T> responseType) {
    String url = config.getUrl() + "?Action=GetPrivateMessage" +
        "&User=" + config.getUser() +
        "&password=" + config.getPassword() +
        "&MessageId=" + messageId;

    return doGet(url, responseType);
  }

}
