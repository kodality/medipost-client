
package com.kodality.medipost.model.common;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.DateField;
import com.kodality.medipost.model.Field;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Patient {

  @JacksonXmlProperty(localName = "IsikukoodiOID")
  private Field personalCodeOID;
  @JacksonXmlProperty(localName = "Isikukood")
  private Field personalCode;
  @JacksonXmlProperty(localName = "PerekonnaNimi")
  private Field lastName;
  @JacksonXmlProperty(localName = "EesNimi")
  private Field firstName;
  @JacksonXmlProperty(localName = "SynniAeg")
  private DateField birthDate;
  @JacksonXmlProperty(localName = "SuguOID")
  private Field sexOID;
  @JacksonXmlProperty(localName = "Sugu")
  private Field sex;
  @JacksonXmlProperty(localName = "Vanus")
  private Age age;
  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  private String error;

  public interface PatientSex {
    String MALE = "M";
    String FEMALE = "N";
    String UNKOWN = "U";
  }

}
