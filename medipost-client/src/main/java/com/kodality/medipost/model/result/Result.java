package com.kodality.medipost.model.result;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.Field;
import com.kodality.medipost.model.common.YesNo;
import com.kodality.medipost.model.observation.ObservationDefGroup;
import com.kodality.medipost.model.common.Organization;
import com.kodality.medipost.model.common.Patient;
import com.kodality.medipost.model.common.Personal;
import com.kodality.medipost.model.order.ReferralDiagnosis;
import com.kodality.medipost.model.observation.Specimen;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Result {
  @JacksonXmlProperty(localName = "ValisTellimuseId")
  private Field customerOrderId;
  @JacksonXmlProperty(localName = "Asutus")
  private List<Organization> organizations;
  @JacksonXmlProperty(localName = "Personal")
  private List<Personal> personal;
  @JacksonXmlProperty(localName = "TellijaMarkused")
  private Field customerComment;
  @JacksonXmlProperty(localName = "SaateDiagnoos")
  private List<ReferralDiagnosis> referralDiagnoses;
  @JacksonXmlProperty(localName = "Patsient")
  private Patient patient;
  @JacksonXmlProperty(localName = "Proov")
  private List<Specimen> specimens;
  @JacksonXmlProperty(localName = "TellimuseNumber")
  private Field labOrderId;
  @JacksonXmlProperty(localName = "LaboriMarkused")
  private Field labComment;
  @JacksonXmlProperty(localName = "TellimuseOlek")
  private ResultStatus orderStatus;
  @JacksonXmlProperty(localName = "UuringuGrupp")
  private List<ObservationDefGroup> observationGroups;
  @JacksonXmlProperty(localName = "cito", isAttribute = true)
  private YesNo cito;

  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  private String error;

}
