
package com.kodality.medipost.model.order;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.Field;
import javax.xml.bind.annotation.XmlElement;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ReferralDiagnosis {

  @JacksonXmlProperty(localName = "DiagnoosiKood")
  private Field code;
  @JacksonXmlProperty(localName = "DiagnoosiNimetus")
  private Field name;
  @XmlElement(name = "Kirjeldus_Diagnoosile")
  private Field description;
  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  private String error;


}
