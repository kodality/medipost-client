package com.kodality.medipost.model.message;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.util.XmlDateTimeDeserializer;
import com.kodality.medipost.util.XmlDateTimeSerializer;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MessageItem {

  @JacksonXmlProperty(localName = "MESSAGEID")
  private String id;
  @JacksonXmlProperty(localName = "RECEIVEDDATETIME")
  @JsonDeserialize(using = XmlDateTimeDeserializer.class)
  @JsonSerialize(using = XmlDateTimeSerializer.class)
  private LocalDateTime date;
  @JacksonXmlProperty(localName = "MESSAGETYPE")
  private String type;
  @JacksonXmlProperty(localName = "SENDER")
  private String sender;
}
