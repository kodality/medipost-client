package com.kodality.medipost.model.observation;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.common.YesNo;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SpecimenBodySite {
  @JacksonXmlProperty(localName = "Paige")
  private List<BodySite> bodySites;
  @JacksonXmlProperty(localName = "Kohustuslik", isAttribute = true)
  private YesNo required;
  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  private String error;
}
