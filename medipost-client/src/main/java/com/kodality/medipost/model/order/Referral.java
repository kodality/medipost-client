
package com.kodality.medipost.model.order;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.Field;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Referral {

  @JacksonXmlProperty(localName = "SaatekirjaNumberOID")
  private Field referralNumberOID;
  @JacksonXmlProperty(localName = "SaatekirjaNumber")
  private Field referralNumber;
  @JacksonXmlProperty(localName = "SaatekirjaTyypOID")
  private Field referralTypeOID;
  @JacksonXmlProperty(localName = "SaatekirjaTyyp")
  private Field referralType;
  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  private String error;

}
