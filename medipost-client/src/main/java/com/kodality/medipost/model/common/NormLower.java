
package com.kodality.medipost.model.common;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class NormLower {

  @JacksonXmlText
  private String content;
  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  private String error;
  @JacksonXmlProperty(localName = "kaasaarvatud", isAttribute = true)
  private YesNo include;

}
