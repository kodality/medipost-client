
package com.kodality.medipost.model.observation;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.DateTimeField;
import com.kodality.medipost.model.Field;
import com.kodality.medipost.model.IntField;
import com.kodality.medipost.model.common.PriceItem;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonPropertyOrder({
    "UuringIdOID",
    "UuringId",
    "TLyhend",
    "KNimetus",
    "UuringNimi",
    "TellijaUuringId",
    "TeostajaUuringId",
    "AsendatavVastusId",
    "TeostajaUuringId",
    "UuringOlek",
    "Mootyhik",
    "Kirjeldus",
    "Jarjekord",
    "MaterjalJarjekorraNr",
    "UuringuElement",
    "Kood",
    "UuringuVastus",
    "UuringuKommentaar",
    "UuringuTaitjaAsutuseJnr",
    "UuringuTaitjaIsikuJnr",
    "ProoviJarjenumber",
    "SisendparameeterId",
})
public class ObservationDefElement {
  @JacksonXmlProperty(localName = "UuringIdOID")
  private Field definitionIdOID;
  @JacksonXmlProperty(localName = "UuringId")
  private Field definitionId;
  /**
   * DO NOT USE CAMEL CASE LIKE THIS 'tCode' - IT BREAKS XML SERIALIZER
   */
  @JacksonXmlProperty(localName = "TLyhend")
  private Field tcode;
  @JacksonXmlProperty(localName = "KNimetus")
  private Field usedName;
  @JacksonXmlProperty(localName = "UuringNimi")
  private Field nameUsedInLab;
  @JacksonXmlProperty(localName = "TellijaUuringId")
  private Field customerObservationId;
  @JacksonXmlProperty(localName = "TeostajaUuringId")
  private Field labDefinitionId;
  @JacksonXmlProperty(localName = "UuringOlek")
  private ObservationStatus status;
  @JacksonXmlProperty(localName = "Kood")
  private List<PriceItem> prices;
  @JacksonXmlProperty(localName = "UuringuVastus")
  private ObservationResult result;
  @JacksonXmlProperty(localName = "UuringuElement")
  private List<ObservationDefElement> elements;
  @JacksonXmlProperty(localName = "ProoviJarjenumber")
  private List<IntField> specimenSequenceNumbers;
  @JacksonXmlProperty(localName = "SisendparameeterId")
  private List<Field> inputParametersIds;
  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  private String error;


  // ON RESULT

  @JacksonXmlProperty(localName = "AsendatavVastusId")
  private Field replacedResultId;
  @JacksonXmlProperty(localName = "Mootyhik")
  private Field measurementUnit;
  @JacksonXmlProperty(localName = "Jarjekord")
  private Field sequence;
  @JacksonXmlProperty(localName = "UuringuKommentaar")
  private Field comment;
  @JacksonXmlProperty(localName = "Lisaja")
  private Field enterer;
  @JacksonXmlProperty(localName = "UuringuTaitjaAsutuseJnr")
  private IntField performerOrganizationSeqNr;
  @JacksonXmlProperty(localName = "UuringuTaitjaIsikuJnr")
  private List<IntField> performerPersonSeqNr;
  @JacksonXmlProperty(localName = "SeotudUuringuVastuseId")
  private Field relatedObservationResultId;
  @JacksonXmlProperty(localName = "TaitmisAlgus")
  private DateTimeField executionStart;
  @JacksonXmlProperty(localName = "TaitmisLopp")
  private DateTimeField executionEnd;


  // ON SERVICE
  @JacksonXmlProperty(localName = "MaterjalJarjekorraNr")
  private IntField specimenDefSequenceNr;
  @JacksonXmlProperty(localName = "SisendTeostamiseks")
  private List<InputParamRelation> inputParamRelations;

}
