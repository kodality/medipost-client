
package com.kodality.medipost.model.order;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.Field;
import com.kodality.medipost.model.common.Organization;
import com.kodality.medipost.model.common.Patient;
import com.kodality.medipost.model.common.Personal;
import com.kodality.medipost.model.common.YesNo;
import com.kodality.medipost.model.observation.InputParameter;
import com.kodality.medipost.model.observation.ObservationDefGroup;
import com.kodality.medipost.model.observation.Specimen;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Order {

  @JacksonXmlProperty(localName = "ValisTellimuseId")
  private Field customerOrderId;
  @JacksonXmlProperty(localName = "Asutus")
  private List<Organization> organizations;
  @JacksonXmlProperty(localName = "Personal")
  private List<Personal> personal;
  @JacksonXmlProperty(localName = "TellijaMarkused")
  private Field customerComment;
  @JacksonXmlProperty(localName = "SaateDiagnoos")
  private List<ReferralDiagnosis> referralDiagnoses;
  @JacksonXmlProperty(localName = "Patsient")
  private Patient patient;
  @JacksonXmlProperty(localName = "Konfidentsiaalsus")
  private Confidentiality confidentiality;
  @JacksonXmlProperty(localName = "Saatekiri")
  private Referral referral;
  @JacksonXmlProperty(localName = "Haigusjuhtum")
  private SicknessCase sicknessCase;
  @JacksonXmlProperty(localName = "KoondIDOID")
  private Field commonIdOID;
  @JacksonXmlProperty(localName = "KoondID")
  private Field commonId;
  @JacksonXmlProperty(localName = "Proov")
  private List<Specimen> specimens;
  @JacksonXmlProperty(localName = "Sisendparameeter")
  private List<InputParameter> inputParameters;
  @JacksonXmlProperty(localName = "UuringuGrupp")
  private List<ObservationDefGroup> observationGroups;
  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  private String error;
  @JacksonXmlProperty(localName = "cito", isAttribute = true)
  private YesNo cito;

}
