package com.kodality.medipost.model.observation;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.Field;
import com.kodality.medipost.model.IntField;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PossibleInputValue {
  @JacksonXmlProperty(localName = "VaartusId")
  private Field id;
  @JacksonXmlProperty(localName = "Vaartus")
  private Field value;
  @JacksonXmlProperty(localName = "VaartusJarjekord")
  private IntField sequenceNumber;
}
