
package com.kodality.medipost.model.order;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.DateTimeField;
import com.kodality.medipost.model.Field;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class Confidentiality {

  @JacksonXmlProperty(localName = "PatsiendileOID")
  private Field patientConfidentialityOID;
  @JacksonXmlProperty(localName = "Patsiendile")
  private Field patientConfidentiality;
  @JacksonXmlProperty(localName = "PatsiendileAvamiseAeg")
  private DateTimeField patientOpenDate;
  @JacksonXmlProperty(localName = "ArstileOID")
  private Field practitionerConfidentialityOID;
  @JacksonXmlProperty(localName = "Arstile")
  private Field practitionerConfidentiality;
  @JacksonXmlProperty(localName = "EsindajaleOID")
  private Field representativeConfidentialityOID;
  @JacksonXmlProperty(localName = "Esindajale")
  private Field representativeConfidentiality;
  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  private String error;


}
