
package com.kodality.medipost.model.notification;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.kodality.medipost.model.MessageType;
import com.kodality.medipost.model.message.BaseMessage;
import com.kodality.medipost.model.message.Header;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
@JacksonXmlRootElement(localName = "Saadetis")
public class NotificationMessage implements BaseMessage {

  @JacksonXmlProperty(localName = "Pais")
  private Header header;

  @JacksonXmlProperty(localName = "Teade")
  private Notification notification;

  @JsonIgnore
  public MessageType getType() {
    return MessageType.Notification;
  }
}
