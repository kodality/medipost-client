package com.kodality.medipost.model.observation;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.DateTimeField;
import com.kodality.medipost.model.Field;
import com.kodality.medipost.model.IntField;
import com.kodality.medipost.model.common.NormLower;
import com.kodality.medipost.model.common.NormUpper;
import com.kodality.medipost.model.common.ResultValue;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ObservationResult {
  @JacksonXmlProperty(localName = "VastuseVaartus")
  protected ResultValue resultValue;
  @JacksonXmlProperty(localName = "VastuseTolgendus")
  protected Field resultInterpretation;
  @JacksonXmlProperty(localName = "SysteemneTolgendus")
  protected SystematicInterpretation systematicInterpretation;
  @JacksonXmlProperty(localName = "VastuseKoodOID")
  protected Field resultCodeOID;
  @JacksonXmlProperty(localName = "VastuseKoodOIDNimi")
  protected Field resultCodeOIDName;
  @JacksonXmlProperty(localName = "VastuseKood")
  protected Field resultCode;
  @JacksonXmlProperty(localName = "VastuseAeg")
  protected DateTimeField resultDate;
  @JacksonXmlProperty(localName = "VastuseSisestajaJnr")
  protected IntField resultEntererSequenceNumber;
  @JacksonXmlProperty(localName = "NormYlem")
  protected NormUpper normUpper;
  @JacksonXmlProperty(localName = "NormAlum")
  protected NormLower normLower;
  @JacksonXmlProperty(localName = "NormiKommentaar")
  protected Field normComment;
  @JacksonXmlProperty(localName = "NormiStaatus")
  protected Field normStatus;
  @JacksonXmlProperty(localName = "ProoviJarjenumber")
  protected List<IntField> specimenSequenceNumber;
  @JacksonXmlProperty(localName = "veakood")
  protected String error;
}
