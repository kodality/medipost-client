package com.kodality.medipost.model.observation;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BodySite {
  @JacksonXmlProperty(localName = "OID", isAttribute = true)
  private String bodySiteOID;
  @JacksonXmlProperty(localName = "Kood", isAttribute = true)
  private String code;
  @JacksonXmlProperty(localName = "Nimetus", isAttribute = true)
  private String name;
}
