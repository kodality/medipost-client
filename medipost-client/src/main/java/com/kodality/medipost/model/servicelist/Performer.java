package com.kodality.medipost.model.servicelist;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.common.Organization;
import com.kodality.medipost.model.observation.InputParameter;
import com.kodality.medipost.model.observation.ObservationDefGroup;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Performer {
  @JacksonXmlProperty(localName = "Asutus")
  private Organization organization;
  @JacksonXmlProperty(localName = "Sisendparameeter")
  private List<InputParameter> inputParameters;
  @JacksonXmlProperty(localName = "UuringuGrupp")
  private List<ObservationDefGroup> definitionGroups;
}
