
package com.kodality.medipost.model.observation;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.Field;
import java.util.List;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class AdditionalInfo {

  @JacksonXmlProperty(localName = "LisaValjaId")
  private Field id;
  @JacksonXmlProperty(localName = "ValjaNimi")
  private Field name;
  @JacksonXmlProperty(localName = "Vaartus")
  private List<Field> value;
  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  private String error;


}
