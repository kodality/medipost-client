package com.kodality.medipost.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import com.kodality.medipost.util.XmlDateTimeDeserializer;
import com.kodality.medipost.util.XmlDateTimeSerializer;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DateTimeField {
  @JacksonXmlText
  @JsonDeserialize(using = XmlDateTimeDeserializer.class)
  @JsonSerialize(using = XmlDateTimeSerializer.class)
  private LocalDateTime content;
  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  private String error;

  public DateTimeField() {

  }

  public DateTimeField(LocalDateTime content) {
    this.content = content;
  }
}
