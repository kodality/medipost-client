package com.kodality.medipost.model.common;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.Field;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Organization {

  @JacksonXmlProperty(localName = "AsutuseId")
  private Field id;
  @JacksonXmlProperty(localName = "AsutuseNimi")
  private Field name;
  @JacksonXmlProperty(localName = "AsutuseKood")
  private Field code;
  @JacksonXmlProperty(localName = "AllyksuseNimi")
  private Field subOrgName;
  @JacksonXmlProperty(localName = "Telefon")
  private Field phone;
  @JacksonXmlProperty(localName = "Vald")
  private Field municipality;
  @JacksonXmlProperty(localName = "Aadress")
  private Field address;
  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  private String error;
  @JacksonXmlProperty(localName = "tyyp", isAttribute = true)
  private String type;
  @JacksonXmlProperty(localName = "jarjenumber", isAttribute = true)
  private Integer sequenceNumber;

  public interface OrganizationType {
    String CUSTOMER = "TELLIJA";
    String PERFORMER = "TEOSTAJA";
  }
}
