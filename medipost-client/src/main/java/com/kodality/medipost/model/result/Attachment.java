package com.kodality.medipost.model.result;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Attachment {
  @JacksonXmlText
  private String content;
  @JacksonXmlProperty(localName = "mediaType", isAttribute = true)
  private String mediaType;
  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  private String error;
}
