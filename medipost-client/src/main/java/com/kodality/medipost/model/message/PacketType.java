package com.kodality.medipost.model.message;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;


@XmlEnum
public enum PacketType {
  @XmlEnumValue("SL") SERVICE_LIST,
  @XmlEnumValue("OL") ORDER,
  @XmlEnumValue("AL") ANSWER,
  @XmlEnumValue("ME") NOTIFICATION;
}

