package com.kodality.medipost.model.message;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JacksonXmlRootElement(localName = "MESSAGES")
public class MessageList {
  @JacksonXmlProperty(localName = "MESSAGE")
  public List<MessageItem> messages;
}
