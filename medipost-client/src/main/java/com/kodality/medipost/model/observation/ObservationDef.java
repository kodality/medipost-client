
package com.kodality.medipost.model.observation;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.DateTimeField;
import com.kodality.medipost.model.Field;
import com.kodality.medipost.model.IntField;
import com.kodality.medipost.model.common.YesNo;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonPropertyOrder({
    "UuringuElement",
    "ProoviJarjenumber",
    "tellitav",
    "veakood",
    "Lisaja",
    "UuringuTaitjaAsutuseJnr",
    "UuringuTaitjaIsikuJnr",
    "SeotudUuringuVastuseId",
    "TaitmisAlgus",
    "TaitmisLopp",
    "UuringuKommentaar",
})
public class ObservationDef {

  @JacksonXmlProperty(localName = "UuringuElement")
  private ObservationDefElement element;
  @JacksonXmlProperty(localName = "ProoviJarjenumber")
  private List<IntField> specimenSequenceNumbers;
  @JacksonXmlProperty(localName = "tellitav", isAttribute = true)
  private YesNo orderable;
  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  private String error;

  // ON RESULT
  @JacksonXmlProperty(localName = "UuringuKommentaar")
  private Field comment;
  @JacksonXmlProperty(localName = "Lisaja")
  private Field enterer;
  @JacksonXmlProperty(localName = "UuringuTaitjaAsutuseJnr")
  private IntField performerOrganizationSeqNr;
  @JacksonXmlProperty(localName = "UuringuTaitjaIsikuJnr")
  private List<IntField> performerPersonSeqNr;
  @JacksonXmlProperty(localName = "SeotudUuringuVastuseId")
  private Field relatedObservationResultId;
  @JacksonXmlProperty(localName = "TaitmisAlgus")
  private DateTimeField executionStart;
  @JacksonXmlProperty(localName = "TaitmisLopp")
  private DateTimeField executionEnd;

  // ON SERVICES
  @JacksonXmlProperty(localName = "MaterjalideGrupp")
  private List<DefinitionSpecimenGroup> definitionSpecimenGroups;

}
