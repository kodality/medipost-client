package com.kodality.medipost.model.notification;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

@XmlEnum
public enum NotificationCode {
  @XmlEnumValue("1") OK,
  @XmlEnumValue("2") PARTIAL_OK,
  @XmlEnumValue("3") XML_SCHEMA_ERROR,
  @XmlEnumValue("4") DATA_ERROR,
  @XmlEnumValue("5") ORDER_LONG_PROCESSING,
  @XmlEnumValue("6") RESTRICTED_MESSAGE,
  @XmlEnumValue("7") UNKNOWN_MESSAGE,
  @XmlEnumValue("8") OTHER;
}
