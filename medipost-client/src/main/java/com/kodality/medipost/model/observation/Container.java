
package com.kodality.medipost.model.observation;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.Field;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class Container {

  @JacksonXmlProperty(localName = "ProovinouKoodOID")
  private Field codeOID;
  @JacksonXmlProperty(localName = "ProovinouKood")
  private Field code;
  @JacksonXmlProperty(localName = "KonteineriNimi")
  private Field name;
  @JacksonXmlProperty(localName = "KonteineriKirjeldus")
  private Field description;
  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  private String error;


}
