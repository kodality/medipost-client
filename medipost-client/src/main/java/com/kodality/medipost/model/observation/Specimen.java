
package com.kodality.medipost.model.observation;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.DateTimeField;
import com.kodality.medipost.model.Field;
import com.kodality.medipost.model.IntField;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Specimen {

  //for example http://pub.e-tervis.ee/oids.py/viewform?oid=1.3.6.1.4.1.28284.1.10.2.21
  @JacksonXmlProperty(localName = "ProovinouIdOID")
  private Field containerIdOID;
  @JacksonXmlProperty(localName = "ProovinouId")
  private Field containerId;
  @JacksonXmlProperty(localName = "MaterjaliTyypOID")
  private Field specimenTypeOID;
  @JacksonXmlProperty(localName = "MaterjaliTyyp")
  private Field specimenType;
  @JacksonXmlProperty(localName = "MaterjaliNimi")
  private Field specimenName;
  @JacksonXmlProperty(localName = "Ribakood")
  private Field barcode;
  @JacksonXmlProperty(localName = "Jarjenumber")
  private IntField sequenceNumber;
  @JacksonXmlProperty(localName = "VotmisAeg")
  private DateTimeField takenDate;
  @JacksonXmlProperty(localName = "ProoviMarkus")
  private Field comment;
  @JacksonXmlProperty(localName = "AdekvaatsusOID")
  private Field adequateOID;
  @JacksonXmlProperty(localName = "Adekvaatsus")
  private Field adequate;
  @JacksonXmlProperty(localName = "PaigeOID")
  private Field bodySiteOID;
  @JacksonXmlProperty(localName = "Paige")
  private Field bodySite;
  @JacksonXmlProperty(localName = "TellijaProovId")
  private Field customerSpecimenId;
  @JacksonXmlProperty(localName = "Konteiner")
  private Container container;
  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  private String error;

  //on result
  @JacksonXmlProperty(localName = "SaabumisAeg")
  private DateTimeField arrivalDate;
}
