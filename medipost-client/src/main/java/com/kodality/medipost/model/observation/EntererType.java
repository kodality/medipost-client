package com.kodality.medipost.model.observation;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

@XmlEnum
public enum EntererType {
  @XmlEnumValue("TELLIJA") AT_ORDERING,
  @XmlEnumValue("MATERJALI_KOGUJA") AT_COLLECTION;
}
