package com.kodality.medipost.model.observation;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SystematicInterpretation {
  @JacksonXmlProperty(localName = "Kood", isAttribute = true)
  private String code;
  @JacksonXmlProperty(localName = "OID", isAttribute = true)
  private String oid;
  @JacksonXmlProperty(localName = "Nimetus", isAttribute = true)
  private String name;
  @JacksonXmlProperty(localName = "Koodistik", isAttribute = true)
  private String codeSystem;
  @JacksonXmlProperty(localName = "URL", isAttribute = true)
  private String url;
  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  private String error;
}
