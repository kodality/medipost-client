package com.kodality.medipost.model.observation;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.common.YesNo;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DefinitionSpecimenGroup {
  @JacksonXmlProperty(localName = "vaikimisi", isAttribute = true)
  private YesNo preferred;
  @JacksonXmlProperty(localName = "Materjal")
  private List<DefinitionSpecimen> definitionSpecimens;

}
