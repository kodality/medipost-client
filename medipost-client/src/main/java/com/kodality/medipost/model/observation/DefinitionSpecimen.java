package com.kodality.medipost.model.observation;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.Field;
import com.kodality.medipost.model.IntField;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DefinitionSpecimen {
  @JacksonXmlProperty(localName = "MaterjaliTyypOID")
  private Field specimenTypeOID;
  @JacksonXmlProperty(localName = "MaterjaliTyyp")
  private Field specimenType;
  @JacksonXmlProperty(localName = "MaterjaliNimi")
  private Field specimenName;
  @JacksonXmlProperty(localName = "KonteineriOmadus")
  private Field containerProperty;

  @JacksonXmlProperty(localName = "MaterjalJarjekord")
  private IntField sequenceNumber;
  @JacksonXmlProperty(localName = "Konteiner")
  private List<Container> containers;
  @JacksonXmlProperty(localName = "MaterjaliPaige")
  private SpecimenBodySite specimenBodySite;
}