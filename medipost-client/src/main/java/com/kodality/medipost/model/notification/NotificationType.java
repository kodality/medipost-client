package com.kodality.medipost.model.notification;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

@XmlEnum
public enum NotificationType {
  @XmlEnumValue("1") STRUCTURE, // not in use
  @XmlEnumValue("2") ORDER,
  @XmlEnumValue("3") ORDER_RESULT;
}
