package com.kodality.medipost.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import com.kodality.medipost.util.XmlDateDeserializer;
import com.kodality.medipost.util.XmlDateSerializer;
import java.time.LocalDate;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DateField {
  @JacksonXmlText
  @JsonDeserialize(using = XmlDateDeserializer.class)
  @JsonSerialize(using = XmlDateSerializer.class)
  private LocalDate content;
  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  private String error;

  public DateField() {

  }

  public DateField(LocalDate content) {
    this.content = content;
  }
}
