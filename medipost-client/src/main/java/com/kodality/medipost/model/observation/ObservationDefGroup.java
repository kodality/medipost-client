
package com.kodality.medipost.model.observation;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.Field;
import com.kodality.medipost.model.IntField;
import com.kodality.medipost.model.common.PriceItem;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ObservationDefGroup {

  @JacksonXmlProperty(localName = "UuringuGruppId")
  private Field definitionGroupId;
  @JacksonXmlProperty(localName = "UuringuGruppNimi")
  private Field definitionGroupName;
  @JacksonXmlProperty(localName = "UuringuGruppJarjekord")
  private IntField sequenceNumber;
  @JacksonXmlProperty(localName = "Uuring")
  private List<ObservationDef> definitions;
  @JacksonXmlProperty(localName = "Lisavali")
  private List<AdditionalInfo> additionalInfo;
  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  private String error;


  // ON RESULT

  @JacksonXmlProperty(localName = "Kood")
  private List<PriceItem> prices;

}
