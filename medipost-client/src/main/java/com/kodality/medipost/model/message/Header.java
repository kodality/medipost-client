
package com.kodality.medipost.model.message;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.Field;
import com.kodality.medipost.model.DateTimeField;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Header {

  @JacksonXmlProperty(localName = "Pakett")
  private Packet packet;
  @JacksonXmlProperty(localName = "Saatja")
  private Field sender;
  @JacksonXmlProperty(localName = "Saaja")
  private Field recipient;
  @JacksonXmlProperty(localName = "Aeg")
  private DateTimeField date;
  @JacksonXmlProperty(localName = "SaadetisId")
  private Field senderMessageId;
  @JacksonXmlProperty(localName = "Email")
  private Field email;

}
