package com.kodality.medipost.model.message;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import javax.xml.bind.annotation.XmlAttribute;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Packet {

  @JacksonXmlText
  private PacketType content;
  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  private String error;
  @XmlAttribute(name = "versioon", required = true)
  private String version;

  public interface Version {
    String _20 = "20";
  }

}
