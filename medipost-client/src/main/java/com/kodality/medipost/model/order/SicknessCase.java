
package com.kodality.medipost.model.order;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.Field;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class SicknessCase {

  @JacksonXmlProperty(localName = "HaigusjuhtumiOID")
  private Field sicknessCaseOID;
  @JacksonXmlProperty(localName = "HaigusjuhtumiNumber")
  private Field sicknessCaseNumber;
  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  private String error;

}
