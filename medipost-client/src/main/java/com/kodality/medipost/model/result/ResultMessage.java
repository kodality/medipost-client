package com.kodality.medipost.model.result;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.kodality.medipost.model.MessageType;
import com.kodality.medipost.model.message.Header;
import com.kodality.medipost.model.message.BaseMessage;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JacksonXmlRootElement(localName = "Saadetis")
public class ResultMessage implements BaseMessage {
  @JacksonXmlProperty(localName = "Pais")
  private Header header;

  @JacksonXmlProperty(localName = "Vastus")
  private Result result;

  @JsonIgnore
  public MessageType getType() {
    return MessageType.Result;
  }
}
