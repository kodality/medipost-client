package com.kodality.medipost.processor;


import com.kodality.medipost.MedipostConfig;
import com.kodality.medipost.client.MedipostClient;
import com.kodality.medipost.client.MedipostClientException;
import com.kodality.medipost.model.MessageType;
import com.kodality.medipost.model.message.MessageItem;
import com.kodality.medipost.model.message.MessageList;
import com.kodality.medipost.model.notification.NotificationMessage;
import com.kodality.medipost.model.order.OrderMessage;
import com.kodality.medipost.model.result.ResultMessage;
import java.util.List;


public abstract class MedipostMessageProcessor {

  private final MedipostClient client;

  protected MedipostMessageProcessor(MedipostConfig config) {
    client = new MedipostClient(config);
  }

  protected MedipostMessageProcessor(MedipostClient client) {
    this.client = client;
  }

  protected void process() {
    MessageList messageList = client.getPrivateMessageList();
    List<MessageItem> messages = messageList.getMessages();
    for (MessageItem item : messages) {
      processMessage(item);
    }
  }

  protected void processMessage(MessageItem item) {
    String messageId = item.getId();
    MessageType messageType = MessageType.fromCode(item.getType());

    try {
      NotificationMessage response = null;
      if (MessageType.Order == messageType) {
        response = handleOrder(client.getOrder(messageId));
      } else if (MessageType.Result == messageType) {
        response = handleResult(client.getResult(messageId));
      } else if (MessageType.Notification == messageType) {
        handleNotification(client.getNotification(messageId));
      }

      client.deletePrivateMessage(messageId);

      if (MessageType.Order == messageType || MessageType.Result == messageType) {
        client.sendNotification(response, item.getSender());
      }
    } catch (MedipostClientException e) {
      handleError(e, item);
    }
  }

  protected abstract void handleError(RuntimeException e, MessageItem messageItem);

  protected abstract NotificationMessage handleOrder(OrderMessage order);

  protected abstract NotificationMessage handleResult(ResultMessage result);

  protected abstract void handleNotification(NotificationMessage notification);


}
