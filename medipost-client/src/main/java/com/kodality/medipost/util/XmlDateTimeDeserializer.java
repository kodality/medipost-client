package com.kodality.medipost.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class XmlDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {

  private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

  @Override
  public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
    String dateTimeString = jsonParser.getValueAsString();
    if (dateTimeString == null || dateTimeString.isBlank()) {
      return null;
    }
    return LocalDateTime.parse(dateTimeString, formatter);
  }
}
