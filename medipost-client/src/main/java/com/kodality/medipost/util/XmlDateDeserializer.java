package com.kodality.medipost.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;
import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XmlDateDeserializer extends JsonDeserializer<LocalDate> {
  @Override
  public LocalDate deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException {
    String dateString = getDate(jsonParser.getValueAsString());
    if (dateString == null || dateString.isEmpty()) {
      return null;
    }
    return LocalDate.parse(dateString);
  }

  private String getDate(String string) {
    if (string == null) {
      return null;
    }

    Pattern p = Pattern.compile("^\\d{4}-\\d{2}-\\d{2}");
    Matcher m = p.matcher(string);
    if (m.find()) {
      return m.group(0);
    }
    return null;
  }
}
